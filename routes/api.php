<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('login', 'UserController@login');

Route::group(['middleware' => 'auth:api'], function(){
    Route::put('/users/{user}/settings','UserController@settings');
    Route::get('/permissions', 'PermissionController@index');
    Route::resource('/colors', 'ColorController');
    Route::resource('/designs', 'DesignController');
    Route::resource('/fabrics', 'FabricController');
    Route::resource('/fabric_colors', 'FabricColorController');
    Route::resource('/fabric_designs', 'FabricDesignController');
    Route::get('/fabric_patterns/{fabricPattern}/stock', 'FabricPatternController@stock');
    Route::post('/fabric_patterns/{fabricPattern}/image', 'FabricPatternController@storeImage');
    Route::delete('/fabric_patterns/{fabricPattern}/image', 'FabricPatternController@destroyImage');
    Route::resource('/fabric_patterns', 'FabricPatternController');
    Route::resource('/customers', 'CustomerController');
    Route::resource('/suppliers', 'SupplierController');
    Route::resource('/stocks', 'StockController');
    Route::post('/purchases/{purchase}/email', 'PurchaseController@sendInvoice');
    Route::get('/purchases/{purchase}/pdf', 'PurchaseController@pdf');
    Route::resource('/purchases', 'PurchaseController');
    Route::resource('/purchase_articles', 'PurchaseArticleController');
    Route::post('/sales/{sale}/email', 'SaleController@sendInvoice');
    Route::get('/sales/{sale}/pdf', 'SaleController@pdf');
    Route::resource('/sales', 'SaleController');
    Route::resource('/sale_articles', 'SaleArticleController');
    Route::resource('/sale_payments', 'SalePaymentController');
    Route::resource('/purchase_payments', 'PurchasePaymentController');
    /*
    Route::middleware('scopes:assistance-management')->resource('/assistance_reports', 'AssistanceReportController');
    Route::resource('/service_reports', 'ServiceReportController');*/
    Route::resource('/user_permissions', 'UserPermissionController');
    Route::resource('/users','UserController');
});

Route::get('/notify', function (Request $request) {
    /*return response()->stream(function () {
        while(true) {
            echo 'data:[]\n\n';
            ob_flush();
            flush();
            sleep(2);
        }
    }, 200, [
        'Content-Type' => 'text/event-stream',
        'X-Accel-Buffering' => 'no',
        'Cache-Control' => 'no-cache',
    ]);*/
    header("Content-Type: text/event-stream");
    header("Cache-Control: no-cache");
    /*while(true) {
        echo 'data:[]\n\n';
        ob_flush();
        flush();
        sleep(2);
    }*/
    // Encode the php array in json format to include it in the response
    $animals = [];
    $data = json_encode($animals);

    echo "data: $data" . "\n\n";
    echo "retry: 1000\n";
    flush();
});
