<?php

namespace App\Http\Controllers;

use DB;
use App\Stock;
use App\Fabric;
use Illuminate\Http\Request;

class StockController extends Controller
{
    public function index(Request $request)
    {
        $query = Fabric::select('*');
        $query->withCount([
            'fabricPattern AS remaining_quantity' => function ($query) {
                $query->join('stocks','stocks.fabric_pattern_id','=','fabric_patterns.id');
                $query->select(DB::raw("SUM(stocks.remaining_quantity) as paidsum"));
            }
        ]);
        /*$query = Stock::with(['fabricPattern.fabric','fabricPattern.color','fabricPattern.design']);
        if (isset($request->search)) {
            $search = json_decode($request->search, true);
            if(isset($search['filters'])){
                $filters = [];
                foreach ($search['filters'] as $key => $filter) {
                    if(!isset($filters[$filter['field']])){
                        $filters[$filter['field']] = [];
                    }
                    $filters[$filter['field']][] = [
                        'value' => $filter['value'],
                        'logic' => isset($filter['logic']) ? $filter['logic'] : '=',
                    ];
                }
                foreach ($filters as $key => $values) {
                    foreach ($values as $index => $value) {
                        if($index == 0){
                            $query->where($key, $values[$index]['logic'], $values[$index]['value']);
                        }
                        else $query->orWhere($key, $values[$index]['logic'], $values[$index]['value']);
                    }
                }
            }
        }
        $query->where('remaining_quantity','>',0);*/
        if (isset($request->paginate)) {
            $paginate = $request->paginate;
            return response()->json($query->paginate($paginate),200);
        }
        return response()->json($query->get(),200);
    }

    public function store(Request $request)
    {
        $stock = Stock::create($request->only([
            'fabric_pattern_id','roll_quantity','quantity','remaining_quantity'
        ]));

        return response()->json([
            'status' => (bool) $stock,
            'data'   => $stock,
            'message' => $stock ? 'Item Created!' : 'Error Creating Item'
        ]);
    }

    public function show(Fabric $stock)
    {
        $query = Fabric::with(['fabricPattern.stock', 'fabricPattern.design', 'fabricPattern.color']);
        /*$query->withCount([
            'fabricPattern AS remaining_quantity' => function ($query) {
                $query->join('stocks','stocks.fabric_pattern_id','=','fabric_patterns.id');
                $query->select(DB::raw("SUM(stocks.remaining_quantity) as paidsum"));
            }
        ]);*/
        return response()->json($query->find($stock->id),200); 
    }

    public function update(Request $request, Stock $stock)
    {
        $status = $stock->update(
            $request->only([
                'fabric_pattern_id','roll_quantity','quantity','remaining_quantity'
            ])
        );

        return response()->json([
            'status' => $status,
            'message' => $status ? 'Item Updated!' : 'Error Updating Item'
        ]);
    }

    public function destroy(Stock $stock)
    {
        $status = $stock->delete();

        return response()->json([
            'status' => $status,
            'message' => $status ? 'Item Deleted!' : 'Error Deleting Item'
        ]);
    }
}
