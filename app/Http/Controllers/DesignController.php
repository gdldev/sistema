<?php

namespace App\Http\Controllers;

use App\Design;
use Illuminate\Http\Request;

class DesignController extends Controller
{
    public function index(Request $request)
    {
        $query = Design::select('*');
        if (isset($request->search)) {
            $search = json_decode($request->search, true);
            if(isset($search['filters'])){
                $filters = [];
                foreach ($search['filters'] as $key => $filter) {
                    if(!isset($filters[$filter['field']])){
                        $filters[$filter['field']] = [];
                    }
                    $filters[$filter['field']][] = [
                        'value' => $filter['value'],
                        'logic' => isset($filter['logic']) ? $filter['logic'] : '=',
                    ];
                }
                foreach ($filters as $key => $values) {
                    foreach ($values as $index => $value) {
                        if($index == 0){
                            $query->where($key, $values[$index]['logic'], $values[$index]['value']);
                        }
                        else $query->orWhere($key, $values[$index]['logic'], $values[$index]['value']);
                    }
                }
            }
        }
        if (isset($request->paginate)) {
            $paginate = $request->paginate;
            return response()->json($query->paginate($paginate),200);
        }
        $query->orderBy('name');
        return response()->json($query->get(),200);
    }

    public function store(Request $request)
    {
        $design = Design::create($request->only(['name']));

        return response()->json([
            'status' => (bool) $design,
            'data'   => $design,
            'message' => $design ? 'Item Created!' : 'Error Creating Item'
        ]);
    }

    public function show(Design $design)
    {
        return response()->json($design,200); 
    }

    public function update(Request $request, Design $design)
    {
        $status = $design->update(
            $request->only(['name'])
        );

        return response()->json([
            'status' => $status,
            'message' => $status ? 'Item Updated!' : 'Error Updating Item'
        ]);
    }

    public function destroy(Design $design)
    {
        $status = $design->delete();

        return response()->json([
            'status' => $status,
            'message' => $status ? 'Item Deleted!' : 'Error Deleting Item'
        ]);
    }
}
