<?php

namespace App\Http\Controllers;

use App\Fabric;
use Illuminate\Http\Request;

class FabricController extends Controller
{
    public function index(Request $request)
    {
        $query = Fabric::select('*');
        if (isset($request->search)) {
            $search = json_decode($request->search, true);
            if(isset($search['filters'])){
                $filters = [];
                foreach ($search['filters'] as $key => $filter) {
                    if(!isset($filters[$filter['field']])){
                        $filters[$filter['field']] = [];
                    }
                    $filters[$filter['field']][] = [
                        'value' => $filter['value'],
                        'logic' => isset($filter['logic']) ? $filter['logic'] : '=',
                    ];
                }
                foreach ($filters as $key => $values) {
                    foreach ($values as $index => $value) {
                        if($index == 0){
                            $query->where($key, $values[$index]['logic'], $values[$index]['value']);
                        }
                        else $query->orWhere($key, $values[$index]['logic'], $values[$index]['value']);
                    }
                }
            }
        }
        if (isset($request->paginate)) {
            $paginate = $request->paginate;
            return response()->json($query->paginate($paginate),200);
        }
        $query->orderBy('name');
        return response()->json($query->get(),200);
    }

    public function store(Request $request)
    {
        $fabric = Fabric::create($request->only(['name','buy_price','sell_price']));

        $colors = $request->colors;
        if(count($colors) > 0){
            $fabric->colors()->createMany($colors);
        }
        
        $designs = $request->designs;
        if(count($designs) > 0){
            $fabric->designs()->createMany($designs);
        }

        return response()->json([
            'status' => (bool) $fabric,
            'data'   => $fabric,
            'message' => $fabric ? 'Item Created!' : 'Error Creating Item'
        ]);
    }

    public function show(Fabric $fabric)
    {
        return response()->json(Fabric::find($fabric->id),200); 
    }

    public function update(Request $request, Fabric $fabric)
    {
        $status = $fabric->update(
            $request->only(['name','buy_price','sell_price'])
        );

        return response()->json([
            'status' => $status,
            'message' => $status ? 'Item Updated!' : 'Error Updating Item'
        ]);
    }

    public function destroy(Fabric $fabric)
    {
        $status = $fabric->delete();

        return response()->json([
            'status' => $status,
            'message' => $status ? 'Item Deleted!' : 'Error Deleting Item'
        ]);
    }
}
