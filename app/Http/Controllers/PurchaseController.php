<?php

namespace App\Http\Controllers;

use PDF, Mail;
use App\NumALetra;
use App\Mail\PurchaseOrder;
use App\Supplier;
use App\Purchase;
use App\Stock;
use Illuminate\Http\Request;

class PurchaseController extends Controller
{
    public function index(Request $request)
    {
        $query = Purchase::selectRaw('purchases.*, IF(currency=1,"MXN","USD") as currency')->with(['supplier','user']);
        $ids = Purchase::select('purchases.id');
        $query->leftJoin('suppliers','suppliers.id','=','purchases.supplier_id');
        $ids->leftJoin('suppliers','suppliers.id','=','purchases.supplier_id');
        if (isset($request->search)) {
            $search = json_decode($request->search, true);
            if(isset($search['filters'])){
                $filters = [];
                foreach ($search['filters'] as $key => $filter) {
                    if(!isset($filters[$filter['field']])){
                        $filters[$filter['field']] = [];
                    }
                    $filters[$filter['field']][] = [
                        'value' => $filter['value'],
                        'logic' => isset($filter['logic']) ? $filter['logic'] : '=',
                    ];
                }
                foreach ($filters as $key => $values) {
                    foreach ($values as $index => $value) {
                        if($index == 0){
                            $query->where($key, $values[$index]['logic'], $values[$index]['value']);
                            $ids->where($key, $values[$index]['logic'], $values[$index]['value']);
                        }
                        else {
                            $query->orWhere($key, $values[$index]['logic'], $values[$index]['value']);
                            $ids->orWhere($key, $values[$index]['logic'], $values[$index]['value']);
                        } 
                    }
                }
            }
        }
        $query->orderBy('created_at','desc');
        if (isset($request->paginate)) {
            $paginate = $request->paginate;
            $totals = collect([
                'totals' => Purchase::selectRaw('
                    SUM(balance) as balance, SUM(total) as total, IF(currency=1,"MXN","USD") as currency
                ')->whereIn('id',$ids->get())->groupBy('currency')->get()
            ]);
            $data = $totals->merge($query->paginate($paginate));
            return response()->json($data,200);
        }
        return response()->json($query->get(),200);
    }

    public function store(Request $request)
    {
        $data = $request->only(['supplier_id','currency'/*,'balance','total'*/]);
        $data['user_id'] = $request->user()->id;

        $purchase = Purchase::create($data);

        if(isset($request->supplier) && is_null($request->supplier_id) 
            && !is_null($request->supplier['name'])){
            $supplier = Supplier::create(['name' => $request->supplier['name']]);
            $purchase->supplier_id = $supplier->id;   
            $purchase->save(); 
        }

        $articles = $request->articles;
        if(count($articles) > 0) {
            $purchase->articles()->createMany($articles);
            if(isset($request->add_stock) && $request->add_stock){
                foreach ($articles as $key => $article) {
                    $article['remaining_quantity'] = $article['quantity'];
                    if(isset($article['fabric_pattern_id'])) Stock::create($article);
                }
            }
            $purchase->total = $purchase->articles->sum('total');
            $purchase->balance = $purchase->total;
            $purchase->save();
        }

        $payments = $request->payments;
        if(count($payments) > 0) {
            $purchase->payments()->createMany($payments);
            $purchase->balance = $purchase->total;
            foreach ($payments as $key => $payment) {
                if($purchase->balance <= 0) {
                    $purchase->balance = 0;
                    break;
                };
                $purchase->balance = $purchase->currency == 1 ? $purchase->balance-$payment['amount_mxn']:$purchase->balance-$payment['amount_usd'];
            }
            $purchase->save();
        }
        
        if($purchase->balance == $purchase->total){
            $purchase->status = 0;
        }else if($purchase->balance < $purchase->total && $purchase->balance > 0){
            $purchase->status = 1;
        }else if($purchase->balance == 0){
            $purchase->status = 2;
        }
        $purchase->save();

        return response()->json([
            'status' => (bool) $purchase,
            'data'   => $purchase,
            'message' => $purchase ? 'Item Created!' : 'Error Creating Item'
        ]);
    }

    public function show(Purchase $purchase)
    {
        return response()->json(Purchase::with([
            'supplier','articles.fabricPattern.fabric','articles.fabricPattern.design','articles.fabricPattern.color','payments'
        ])->find($purchase->id),200); 
    }

    public function update(Request $request, Purchase $purchase)
    {
        $status = $purchase->update(
            $request->only([
                'supplier_id','currency'/*,'balance','total'*/
            ])
        );

        $articles = $purchase->articles;
        if(count($articles) > 0) {
            $purchase->total = $purchase->articles->sum('total');
            $purchase->balance = $purchase->total;
            $purchase->save();
        }

        $payments = $purchase->payments;
        if(count($payments) > 0) {
            $purchase->balance = $purchase->total;
            foreach ($payments as $key => $payment) {
                if($purchase->balance <= 0) {
                    $purchase->balance = 0;
                    break;
                };
                $purchase->balance = $purchase->currency == 1 ? $purchase->balance-$payment['amount_mxn']:$purchase->balance-$payment['amount_usd'];
            }
            $purchase->save();
        }

        if($purchase->balance == $purchase->total){
            $purchase->status = 0;
        }else if($purchase->balance < $purchase->total && $purchase->balance > 0){
            $purchase->status = 1;
        }else if($purchase->balance == 0){
            $purchase->status = 2;
        }
        $purchase->save();

        return response()->json([
            'status' => $status,
            'message' => $status ? 'Item Updated!' : 'Error Updating Item'
        ]);
    }

    public function destroy(Purchase $purchase)
    {
        $status = $purchase->delete();

        return response()->json([
            'status' => $status,
            'message' => $status ? 'Item Deleted!' : 'Error Deleting Item'
        ]);
    }
    
    public function pdf(Purchase $purchase) {
        $purchase = Purchase::with([
            'articles.fabricPattern.fabric','articles.fabricPattern.design','articles.fabricPattern.color','supplier','payments'
        ])->find($purchase->id);
        
        $purchase->total_amount = NumALetra::numeroALetra($purchase->total,$purchase->currency);

        $purchase->due_date = date("d/m/Y",strtotime($purchase->created_at));
        $purchase->dia = date("d",strtotime($purchase->created_at));
        $purchase->mes = NumALetra::obtenerMesConLetra($purchase->created_at);
        $purchase->anio = date("Y",strtotime($purchase->created_at));
        
        $pdf = PDF::loadView('purchase',['data' => $purchase]);
        return $pdf->stream('');
    }
    
    public function sendInvoice(Request $request, Purchase $purchase)
    {
        //return new PurchaseOrder($purchase);
        $recipients = $request->recipients;
        $pdf = $this->pdf($purchase);
        $mail = Mail::to($recipients[0]);
        if(count($recipients) > 1){
            unset($recipients[0]);
            $mail->cc($recipients);
        }
        $mail->send(new PurchaseOrder($purchase, $pdf));

        $status = count(Mail::failures()) == 0;
        return response()->json([
            'status' => $status,
            'data' => Mail::failures(),
            'message' => $status ? 'Invoice Sended!' : 'Error Sending Invoice'
        ]);
    }
}
