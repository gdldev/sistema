<?php

namespace App\Http\Controllers;

use App\PurchaseArticle;
use Illuminate\Http\Request;

class PurchaseArticleController extends Controller
{
    public function store(Request $request)
    {
        $purchaseArticle = PurchaseArticle::create($request->only([
            'purchase_id','fabric_pattern_id','description','roll_quantity','quantity','price','total'
        ]));

        $purchase = $purchaseArticle->purchase;
        $articles = $purchase->articles;
        if(count($articles) > 0) {
            $purchase->total = $purchase->articles->sum('total');
            $purchase->balance = $purchase->total;
            $purchase->save();
        }

        $payments = $purchase->payments;
        if(count($payments) > 0) {
            $purchase->balance = $purchase->total;
            foreach ($payments as $key => $payment) {
                if($purchase->balance <= 0) {
                    $purchase->balance = 0;
                    break;
                };
                $purchase->balance = $purchase->currency == 1 ? $purchase->balance-$payment['amount_mxn']:$purchase->balance-$payment['amount_usd'];
            }
            $purchase->save();
        }

        if($purchase->balance == $purchase->total){
            $purchase->status = 0;
        }else if($purchase->balance < $purchase->total && $purchase->balance > 0){
            $purchase->status = 1;
        }else if($purchase->balance == 0){
            $purchase->status = 2;
        }
        $purchase->save();

        return response()->json([
            'status' => (bool) $purchaseArticle,
            'data'   => $purchaseArticle,
            'message' => $purchaseArticle ? 'Item Created!' : 'Error Creating Item'
        ]);
    }

    public function destroy(PurchaseArticle $purchaseArticle)
    {
        $purchase = $purchaseArticle->purchase;

        $status = $purchaseArticle->delete();
        
        $articles = $purchase->articles;
        if(count($articles) > 0) {
            $purchase->total = $purchase->articles->sum('total');
            $purchase->balance = $purchase->total;
            $purchase->save();
        }

        $payments = $purchase->payments;
        if(count($payments) > 0) {
            $purchase->balance = $purchase->total;
            foreach ($payments as $key => $payment) {
                if($purchase->balance <= 0) {
                    $purchase->balance = 0;
                    break;
                };
                $purchase->balance = $purchase->currency == 1 ? $purchase->balance-$payment['amount_mxn']:$purchase->balance-$payment['amount_usd'];
            }
            $purchase->save();
        }
        
        if($purchase->balance == $purchase->total){
            $purchase->status = 0;
        }else if($purchase->balance < $purchase->total && $purchase->balance > 0){
            $purchase->status = 1;
        }else if($purchase->balance == 0){
            $purchase->status = 2;
        }
        $purchase->save();

        return response()->json([
            'status' => $status,
            'message' => $status ? 'Item Deleted!' : 'Error Deleting Item'
        ]);
    }
}
