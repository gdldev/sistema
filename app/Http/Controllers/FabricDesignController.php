<?php

namespace App\Http\Controllers;

use App\FabricDesign;
use Illuminate\Http\Request;

class FabricDesignController extends Controller
{
    public function store(Request $request)
    {
        $fabricDesign = FabricDesign::create($request->only(['fabric_id','design_id']));

        return response()->json([
            'status' => (bool) $fabricDesign,
            'data'   => $fabricDesign,
            'message' => $fabricDesign ? 'Item Created!' : 'Error Creating Item'
        ]);
    }

    public function destroy(FabricDesign $fabricDesign)
    {
        $status = $fabricDesign->delete();

        return response()->json([
            'status' => $status,
            'message' => $status ? 'Item Deleted!' : 'Error Deleting Item'
        ]);
    }
}
