<?php

namespace App\Http\Controllers;

use PDF, Mail;
use App\NumALetra;
use App\Mail\Invoice;
use DB;
use App\Sale;
use App\Stock;
use App\Customer;
use Illuminate\Http\Request;

class SaleController extends Controller
{
    public function index(Request $request)
    {
        $query = Sale::selectRaw('sales.*, IF(currency=1,"MXN","USD") as currency')->with(['customer','user']);
        $ids = Sale::select('sales.id');
        $query->leftJoin('customers','customers.id','=','sales.customer_id');
        $ids->leftJoin('customers','customers.id','=','sales.customer_id');
        if (isset($request->search)) {
            $search = json_decode($request->search, true);
            if(isset($search['filters'])){
                $filters = [];
                foreach ($search['filters'] as $key => $filter) {
                    if(!isset($filters[$filter['field']])){
                        $filters[$filter['field']] = [];
                    }
                    $filters[$filter['field']][] = [
                        'value' => $filter['value'],
                        'logic' => isset($filter['logic']) ? $filter['logic'] : '=',
                    ];
                }
                foreach ($filters as $key => $values) {
                    foreach ($values as $index => $value) {
                        if($index == 0){
                            $query->where($key, $values[$index]['logic'], $values[$index]['value']);
                            $ids->where($key, $values[$index]['logic'], $values[$index]['value']);
                        }
                        else {
                            $query->orWhere($key, $values[$index]['logic'], $values[$index]['value']);
                            $ids->orWhere($key, $values[$index]['logic'], $values[$index]['value']);
                        } 
                    }
                }
            }
        }
        if($request->user()->user_type == 3 && !is_null($request->user()->customer_id)){
            $query->where('customer_id',$request->user()->customer_id);
            $ids->where('customer_id',$request->user()->customer_id);
        }
        else if($request->user()->user_type == 3 && is_null($request->user()->customer_id)){
            return response()->json(null,403);
        }
        if($request->user()->user_type == 2){
            $query->where('user_id',$request->user()->id);
            $ids->where('user_id',$request->user()->id);
        }
        $query->orderBy('created_at','desc');
        if (isset($request->paginate)) {
            $paginate = $request->paginate;
            $totals = collect([
                'totals' => Sale::selectRaw('
                    SUM(revenue) as revenue, SUM(commission) as commission, SUM(balance) as balance, SUM(total) as total, IF(currency=1,"MXN","USD") as currency
                ')->whereIn('id',$ids->get())->groupBy('currency')->get()
            ]);
            $data = $totals->merge($query->paginate($paginate));
            return response()->json($data,200);
        }
        return response()->json($query->get(),200);
    }

    public function store(Request $request)
    {
        if(isset($request->update_stock) && $request->update_stock){
            if(count($request->articles) > 0){
                $articles_error = [];
                DB::beginTransaction();
                foreach ($request->articles as $value) {
                    if(isset($value['fabric_pattern_id']) && !is_null($value['fabric_pattern_id'])){
                        $stocks = Stock::where('fabric_pattern_id',$value['fabric_pattern_id'])
                            ->where('remaining_quantity', '>' ,0)->get();
                        $stock = Stock::where('fabric_pattern_id',$value['fabric_pattern_id'])->sum('remaining_quantity');
                        if($value['quantity'] > $stock){
                            $value['current_stock'] = $stock;
                            $articles_error[] = $value;
                        } else {
                            $quantity = $value['quantity'];
                            foreach ($stocks as $pattern) {
                                if($quantity > 0){
                                    if($quantity > $pattern['remaining_quantity']){
                                        $quantity -= $pattern['remaining_quantity'];
                                        $pattern['remaining_quantity'] = 0;
                                    }
                                    else{
                                        $pattern['remaining_quantity'] -= $quantity;
                                    }
                                    $pattern->save();
                                }
                            }
                        }
                    }
                }
                if(count($articles_error) > 0){
                    DB::rollBack();
                    return response()->json([
                        'status' => false,
                        'data'   => $articles_error,
                        'message' => 'Not enough stock for some patterns',
                        'code' => 201
                    ],401);
                }
                else DB::commit();
            }
        }

        $data = $request->only(['customer_id','currency',/*'balance','total','revenue','commission'*/]);
        $data['user_id'] = $request->user()->id;

        $sale = Sale::create($data);

        if(isset($request->customer) && is_null($request->customer_id) 
            && !is_null($request->customer['name'])){
            $customer = Customer::create(['name' => $request->customer['name']]);
            $sale->customer_id = $customer->id;    
            $sale->save(); 
        }
        
        $articles = $request->articles;
        if(count($articles) > 0) {
            $sale->articles()->createMany($articles);
            $sale->total = $sale->articles->sum('total');
            $sale->balance = $sale->total;
            $sale->revenue = $sale->articles->sum('revenue');
            $sale->commission = $sale->articles->sum('commission');
            $sale->save();
        }

        $payments = $request->payments;
        if(count($payments) > 0) {
            $sale->payments()->createMany($payments);
            $sale->balance = $sale->total;
            foreach ($payments as $key => $payment) {
                if($sale->balance <= 0) {
                    $sale->balance = 0;
                    break;
                };
                $sale->balance = $sale->currency == 1 ? $sale->balance-$payment['amount_mxn']:$sale->balance-$payment['amount_usd'];
            }
            $sale->save();
        }
        
        if($sale->balance == $sale->total){
            $sale->status = 0;
        }else if($sale->balance < $sale->total && $sale->balance > 0){
            $sale->status = 1;
        }else if($sale->balance == 0){
            $sale->status = 2;
        }
        $sale->save();

        return response()->json([
            'status' => (bool) $sale,
            'data'   => $sale,
            'message' => $sale ? 'Item Created!' : 'Error Creating Item'
        ]);
    }

    public function show(Request $request, Sale $sale)
    {
        if($request->user()->user_type == 3 && $request->user()->customer_id !=$sale->customer_id){
            return response()->json(null,403);
        }
        if($request->user()->user_type == 2 && $request->user()->id !=$sale->user_id){
            return response()->json(null,403);
        }
        return response()->json(Sale::with([
            'customer','articles.fabricPattern.fabric','articles.fabricPattern.design','articles.fabricPattern.color','payments','articles.user'
        ])->find($sale->id),200); 
    }

    public function update(Request $request, Sale $sale)
    {
        $status = $sale->update(
            $request->only([
                'customer_id','currency',/*'balance','total','revenue','commission'*/
            ])
        );

        $articles = $sale->articles;
        if(count($articles) > 0) {
            $sale->total = $sale->articles->sum('total');
            $sale->balance = $sale->total;
            $sale->revenue = $sale->articles->sum('revenue');
            $sale->commission = $sale->articles->sum('commission');
            $sale->save();
        }

        $payments = $sale->payments;
        if(count($payments) > 0) {
            $sale->balance = $sale->total;
            foreach ($payments as $key => $payment) {
                if($sale->balance <= 0) {
                    $sale->balance = 0;
                    break;
                };
                $sale->balance = $sale->currency == 1 ? $sale->balance-$payment['amount_mxn']:$sale->balance-$payment['amount_usd'];
            }
            $sale->save();
        }

        if($sale->balance == $sale->total){
            $sale->status = 0;
        }else if($sale->balance < $sale->total && $sale->balance > 0){
            $sale->status = 1;
        }else if($sale->balance == 0){
            $sale->status = 2;
        }
        $sale->save();

        return response()->json([
            'status' => $status,
            'message' => $status ? 'Item Updated!' : 'Error Updating Item'
        ]);
    }

    public function destroy(Sale $sale)
    {
        $status = $sale->delete();

        return response()->json([
            'status' => $status,
            'message' => $status ? 'Item Deleted!' : 'Error Deleting Item'
        ]);
    }

    public function pdf(Sale $sale) {
        $sale = Sale::with([
            'articles.fabricPattern.fabric','articles.fabricPattern.design','articles.fabricPattern.color','customer','payments'
        ])->find($sale->id);
        
        $sale->total_amount = NumALetra::numeroALetra($sale->total,$sale->currency);

        $sale->due_date = date("d/m/Y",strtotime($sale->created_at));
        if(!is_null($sale->customer) && !is_null($sale->customer->credit_days)) {
            $sale->due_date=date("d/m/Y",strtotime($sale->created_at.' + '.$sale->customer->credit_days.' day'));
        }
        $sale->dia = date("d",strtotime($sale->created_at));
        $sale->mes = NumALetra::obtenerMesConLetra($sale->created_at);
        $sale->anio = date("Y",strtotime($sale->created_at));
        
        $pdf = PDF::loadView('invoice',['data' => $sale]);
        return $pdf->stream('');
    }
    
    public function sendInvoice(Request $request, Sale $sale)
    {
        //return new Invoice($sale);
        $recipients = $request->recipients;
        $pdf = $this->pdf($sale);
        $mail = Mail::to($recipients[0]);
        if(count($recipients) > 1){
            unset($recipients[0]);
            $mail->cc($recipients);
        }
        $mail->send(new Invoice($sale, $pdf));

        $status = count(Mail::failures()) == 0;
        return response()->json([
            'status' => $status,
            'data' => Mail::failures(),
            'message' => $status ? 'Invoice Sended!' : 'Error Sending Invoice'
        ]);
    }
}
