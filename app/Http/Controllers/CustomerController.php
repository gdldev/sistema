<?php

namespace App\Http\Controllers;

use Validator;
use App\User;
use App\Customer;
use Illuminate\Http\Request;

class CustomerController extends Controller
{
    public function index(Request $request)
    {
        $query = Customer::select('customers.*');
        if (isset($request->search)) {
            $search = json_decode($request->search, true);
            if(isset($search['filters'])){
                $filters = [];
                foreach ($search['filters'] as $key => $filter) {
                    if(!isset($filters[$filter['field']])){
                        $filters[$filter['field']] = [];
                    }
                    $filters[$filter['field']][] = [
                        'value' => $filter['value'],
                        'logic' => isset($filter['logic']) ? $filter['logic'] : '=',
                    ];
                }
                foreach ($filters as $key => $values) {
                    foreach ($values as $index => $value) {
                        if($index == 0){
                            $query->where($key, $values[$index]['logic'], $values[$index]['value']);
                        }
                        else $query->orWhere($key, $values[$index]['logic'], $values[$index]['value']);
                    }
                }
            }
        }
        if(isset($request->balance) && $request->balance){
            $query->with(['sales','balance']);
            $query->join('sales','sales.customer_id','=','customers.id');
            $query->where('sales.deleted_at',NULL);
            $query->where('sales.balance','>',0);
            $query->groupBy('customers.id');
        }
        $query->orderBy('name');
        if (isset($request->paginate)) {
            $paginate = $request->paginate;
            return response()->json($query->paginate($paginate),200);
        }
        return response()->json($query->get(),200);
    }

    public function generatePassword($length) {
        $key = '';
        $keys = array_merge(range(0, 9), range('a', 'z'));
    
        for ($i = 0; $i < $length; $i++) {
            $key .= $keys[array_rand($keys)];
        }
    
        return $key;
    }

    public function store(Request $request)
    {
        if(isset($request->account) && $request->account){
            $validator = Validator::make($request->all(), [
                'name' => 'required|max:50',
                'email' => 'required|email'
            ]);
            if ($validator->fails()) {
                return response()->json(['error' => $validator->errors()], 401);
            }
        }

        $customer = Customer::create($request->only([
            'name','address','telephone','mobilephone','email','credit_days','comments'
        ]));
        $user = [];
        $password = '';
        if(isset($request->account) && $request->account){
            $data = $request->only(['name', 'email']);
            $password = $this->generatePassword(8);
            $data['password'] = bcrypt($password);
            $data['user_type'] = 3;
            $data['customer_id'] = $customer->id;
            $user = User::create($data);
        }
        return response()->json([
            'status' => (bool) $customer,
            'data'   => $customer,
            'password'   => $password,
            'user'   => $user,
            'message' => $customer ? 'Item Created!' : 'Error Creating Item'
        ]);
    }

    public function show(Request $request, Customer $customer)
    {
        if(isset($request->balance) && $request->balance){
            return response()->json(Customer::with(['sales.user'])->find($customer->id),200); 
        }
        return response()->json($customer,200); 
    }

    public function update(Request $request, Customer $customer)
    {
        $status = $customer->update(
            $request->only([
                'name','address','telephone','mobilephone','email','credit_days','comments'
            ])
        );

        return response()->json([
            'status' => $status,
            'message' => $status ? 'Item Updated!' : 'Error Updating Item'
        ]);
    }

    public function destroy(Customer $customer)
    {
        $status = $customer->delete();

        return response()->json([
            'status' => $status,
            'message' => $status ? 'Item Deleted!' : 'Error Deleting Item'
        ]);
    }
}
