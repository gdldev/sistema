<?php

namespace App\Http\Controllers;

use App\PurchasePayment;
use Illuminate\Http\Request;

class PurchasePaymentController extends Controller
{
    public function store(Request $request)
    {
        $purchasePayment = PurchasePayment::create([
            'purchase_id' => $request->purchase_id,
            'amount_mxn' => $request->amount_mxn,
            'amount_usd' => $request->amount_usd,
        ]);

        $purchase = $purchasePayment->purchase;
        $purchase->balance = $purchase->currency == 1 ? $purchase->balance-$purchasePayment->amount_mxn:$purchase->balance-$purchasePayment->amount_usd;
        if($purchase->total == 0) $purchase->balance = 0;
        if($purchase->balance < 0) $purchase->balance = 0;
        $purchase->save();

        return response()->json([
            'status' => (bool) $purchasePayment,
            'data'   => $purchasePayment,
            'message' => $purchasePayment ? 'Item Added!' : 'Error Adding Item'
        ]);
    }

    public function destroy(PurchasePayment $purchasePayment)
    {

        $purchase = $purchasePayment->purchase;
        $purchase->balance = $purchase->currency == 1 ? $purchase->balance+$purchasePayment->amount_mxn:$purchase->balance+$purchasePayment->amount_usd;
        if($purchase->total == 0) $purchase->balance = 0;
        $purchase->save();

        $status = $purchasePayment->delete();

        return response()->json([
            'status' => $status,
            'message' => $status ? 'Item Deleted!' : 'Error Deleting Item'
        ]);
    }
}
