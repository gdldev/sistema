<?php

namespace App\Http\Controllers;

use App\FabricColor;
use Illuminate\Http\Request;

class FabricColorController extends Controller
{
    public function store(Request $request)
    {
        $fabricColor = FabricColor::create($request->only(['fabric_id','color_id']));

        return response()->json([
            'status' => (bool) $fabricColor,
            'data'   => $fabricColor,
            'message' => $fabricColor ? 'Item Created!' : 'Error Creating Item'
        ]);
    }

    public function destroy(FabricColor $fabricColor)
    {
        $status = $fabricColor->delete();

        return response()->json([
            'status' => $status,
            'message' => $status ? 'Item Deleted!' : 'Error Deleting Item'
        ]);
    }
}
