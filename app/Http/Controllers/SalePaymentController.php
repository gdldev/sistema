<?php

namespace App\Http\Controllers;

use App\SalePayment;
use Illuminate\Http\Request;

class SalePaymentController extends Controller
{
    public function store(Request $request)
    {
        $salePayment = SalePayment::create([
            'sale_id' => $request->sale_id,
            'amount_mxn' => $request->amount_mxn,
            'amount_usd' => $request->amount_usd,
        ]);

        $sale = $salePayment->sale;
        $sale->balance = $sale->currency == 1 ? $sale->balance-$salePayment->amount_mxn:$sale->balance-$salePayment->amount_usd;
        if($sale->total == 0) $sale->balance = 0;
        if($sale->balance < 0) $sale->balance = 0;
        $sale->save();

        return response()->json([
            'status' => (bool) $salePayment,
            'data'   => $salePayment,
            'message' => $salePayment ? 'Item Added!' : 'Error Adding Item'
        ]);
    }

    public function destroy(SalePayment $salePayment)
    {

        $sale = $salePayment->sale;
        $sale->balance = $sale->currency == 1 ? $sale->balance+$salePayment->amount_mxn:$sale->balance+$salePayment->amount_usd;
        if($sale->total == 0) $sale->balance = 0;
        $sale->save();

        $status = $salePayment->delete();

        return response()->json([
            'status' => $status,
            'message' => $status ? 'Item Deleted!' : 'Error Deleting Item'
        ]);
    }
}
