<?php

namespace App\Http\Controllers;

use Auth;
use App\SaleArticle;
use Illuminate\Http\Request;

class SaleArticleController extends Controller
{
    public function store(Request $request)
    {
        $saleArticle = SaleArticle::create($request->only([
            'sale_id','fabric_pattern_id','description','quantity','price','total','cancelled_by'
        ]));

        $sale = $saleArticle->sale;
        $articles = $sale->articles;
        if(count($articles) > 0) {
            $sale->total = $sale->articles->sum('total');
            $sale->balance = $sale->total;
            $sale->revenue = $sale->articles->sum('revenue');
            $sale->commission = $sale->articles->sum('commission');
            $sale->save();
        }

        $payments = $sale->payments;
        if(count($payments) > 0) {
            $sale->balance = $sale->total;
            foreach ($payments as $key => $payment) {
                if($sale->balance <= 0) {
                    $sale->balance = 0;
                    break;
                };
                $sale->balance = $sale->currency == 1 ? $sale->balance-$payment['amount_mxn']:$sale->balance-$payment['amount_usd'];
            }
            $sale->save();
        }

        if($sale->balance == $sale->total){
            $sale->status = 0;
        }else if($sale->balance < $sale->total && $sale->balance > 0){
            $sale->status = 1;
        }else if($sale->balance == 0){
            $sale->status = 2;
        }
        $sale->save();

        return response()->json([
            'status' => (bool) $saleArticle,
            'data'   => $saleArticle,
            'message' => $saleArticle ? 'Item Created!' : 'Error Creating Item'
        ]);
    }

    public function destroy(SaleArticle $saleArticle)
    {
        $sale = $saleArticle->sale;

        $saleArticle->cancelled_by = Auth::user()->id;
        $saleArticle->save();
        $status = $saleArticle->delete();
        
        $articles = $sale->articles;
        if(count($articles) > 0) {
            $sale->total = $sale->articles->sum('total');
            $sale->balance = $sale->total;
            $sale->revenue = $sale->articles->sum('revenue');
            $sale->commission = $sale->articles->sum('commission');
            $sale->save();
        }

        $payments = $sale->payments;
        if(count($payments) > 0) {
            $sale->balance = $sale->total;
            foreach ($payments as $key => $payment) {
                if($sale->balance <= 0) {
                    $sale->balance = 0;
                    break;
                };
                $sale->balance = $sale->currency == 1 ? $sale->balance-$payment['amount_mxn']:$sale->balance-$payment['amount_usd'];
            }
            $sale->save();
        }
        
        if($sale->balance == $sale->total){
            $sale->status = 0;
        }else if($sale->balance < $sale->total && $sale->balance > 0){
            $sale->status = 1;
        }else if($sale->balance == 0){
            $sale->status = 2;
        }
        $sale->save();
        
        return response()->json([
            'status' => $status,
            'message' => $status ? 'Item Deleted!' : 'Error Deleting Item'
        ]);
    }
}
