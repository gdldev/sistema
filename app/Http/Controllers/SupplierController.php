<?php

namespace App\Http\Controllers;

use App\Supplier;
use Illuminate\Http\Request;

class SupplierController extends Controller
{
    public function index(Request $request)
    {
        $query = Supplier::select('suppliers.*');
        if (isset($request->search)) {
            $search = json_decode($request->search, true);
            if(isset($search['filters'])){
                $filters = [];
                foreach ($search['filters'] as $key => $filter) {
                    if(!isset($filters[$filter['field']])){
                        $filters[$filter['field']] = [];
                    }
                    $filters[$filter['field']][] = [
                        'value' => $filter['value'],
                        'logic' => isset($filter['logic']) ? $filter['logic'] : '=',
                    ];
                }
                foreach ($filters as $key => $values) {
                    foreach ($values as $index => $value) {
                        if($index == 0){
                            $query->where($key, $values[$index]['logic'], $values[$index]['value']);
                        }
                        else $query->orWhere($key, $values[$index]['logic'], $values[$index]['value']);
                    }
                }
            }
        }
        if(isset($request->balance) && $request->balance){
            $query->with(['purchases','balance']);
            $query->join('purchases','purchases.supplier_id','=','suppliers.id');
            $query->where('purchases.deleted_at',NULL);
            $query->where('purchases.balance','>',0);
            $query->groupBy('suppliers.id');
        }
        $query->orderBy('name');
        if (isset($request->paginate)) {
            $paginate = $request->paginate;
            return response()->json($query->paginate($paginate),200);
        }
        return response()->json($query->get(),200);
    }

    public function store(Request $request)
    {
        $supplier = Supplier::create($request->only([
            'name','address','telephone','mobilephone','email','comments'
        ]));

        return response()->json([
            'status' => (bool) $supplier,
            'data'   => $supplier,
            'message' => $supplier ? 'Item Created!' : 'Error Creating Item'
        ]);
    }

    public function show(Request $request, Supplier $supplier)
    {
        if(isset($request->balance) && $request->balance){
            return response()->json(Supplier::with(['purchases.user'])->find($supplier->id),200); 
        }
        return response()->json($supplier,200); 
    }

    public function update(Request $request, Supplier $supplier)
    {
        $status = $supplier->update(
            $request->only([
                'name','address','telephone','mobilephone','email','comments'
            ])
        );

        return response()->json([
            'status' => $status,
            'message' => $status ? 'Item Updated!' : 'Error Updating Item'
        ]);
    }

    public function destroy(Supplier $supplier)
    {
        $status = $supplier->delete();

        return response()->json([
            'status' => $status,
            'message' => $status ? 'Item Deleted!' : 'Error Deleting Item'
        ]);
    }
}
