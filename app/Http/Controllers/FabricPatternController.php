<?php

namespace App\Http\Controllers;

use Storage;
use App\FabricPattern;
use Illuminate\Http\Request;

class FabricPatternController extends Controller
{
    public function index(Request $request)
    {
        $query = FabricPattern::with(['fabric','color','design']);
        if (isset($request->search)) {
            $search = json_decode($request->search, true);
            if(isset($search['filters'])){
                $filters = [];
                foreach ($search['filters'] as $key => $filter) {
                    if(!isset($filters[$filter['field']])){
                        $filters[$filter['field']] = [];
                    }
                    $filters[$filter['field']][] = [
                        'value' => $filter['value'],
                        'logic' => isset($filter['logic']) ? $filter['logic'] : '=',
                    ];
                }
                foreach ($filters as $key => $values) {
                    foreach ($values as $index => $value) {
                        if($index == 0){
                            $query->where($key, $values[$index]['logic'], $values[$index]['value']);
                        }
                        else $query->orWhere($key, $values[$index]['logic'], $values[$index]['value']);
                    }
                }
            }
            if(isset($search['name'])){
                $query->selectRaw('
                    fabric_patterns.*, 
                    CONCAT(
                        fabric_patterns.sku," ",fabrics.name," ",IFNULL(designs.name,"")," ",IFNULL(colors.name,"")
                    ) as name'
                );
                $query->join('fabrics','fabrics.id','=','fabric_patterns.fabric_id');
                $query->leftJoin('colors','colors.id','=','fabric_patterns.color_id');
                $query->leftJoin('designs','designs.id','=','fabric_patterns.design_id');
                $query->having('name','like','%'.$search['name'].'%');
            }
        }
        if (isset($request->paginate)) {
            $paginate = $request->paginate;
            return response()->json($query->paginate($paginate),200);
        }
        return response()->json($query->get(),200);
    }

    public function store(Request $request)
    {
        $fabricPattern = FabricPattern::create([
            'sku' => $request->sku,
            'fabric_id' => $request->fabric_id,
            'color_id' => $request->color_id,
            'design_id' => $request->design_id,
        ]);
        
        if(isset($request->image)){
            $file_data = $request->image;
            $file_name = $fabricPattern->id.'_'.time().'.png'; //generating unique file name; 
            list($type, $file_data) = explode(';', $file_data);
            list(, $file_data) = explode(',', $file_data); 
            if($file_data!=""){ // storing image in storage/app/public Folder 
                Storage::disk('public')->put('\\fabrics\\'.$file_name,base64_decode($file_data));
                $fabricPattern->image = '\\storage\\fabrics\\'.$file_name;
                $fabricPattern->save();
            }
        }

        return response()->json([
            'status' => (bool) $fabricPattern,
            'data'   => $fabricPattern,
            'message' => $fabricPattern ? 'Item Added!' : 'Error Adding Item'
        ]);
    }

    public function storeImage(FabricPattern $fabricPattern, Request $request){
        if(isset($request->image)){
            $path = '\\storage\\fabrics\\';
            $file = $request->image->store('fabrics','public');
            if((bool) $file){
                $fabricPattern->image = '\\storage\\'.$file;
                $fabricPattern->save();
            }
        }
        return response()->json([
            'status' => (bool) $fabricPattern,
            'data'   => $fabricPattern,
            'message' => $fabricPattern ? 'Image Added!' : 'Error Adding Image'
        ]);
    }
    
    public function show(FabricPattern $fabricPattern)
    {
        return response()->json($fabricPattern,200); 
    }

    public function update(Request $request, FabricPattern $fabricPattern)
    {
        $status = $fabricPattern->update(
            $request->only(['sku','fabric_id','color_id','design_id'])
        );

        return response()->json([
            'status' => $status,
            'message' => $status ? 'Item Updated!' : 'Error Updating Item'
        ]);
    }

    public function destroyImage(FabricPattern $fabricPattern){
        $file_name = str_replace('\storage',"",$fabricPattern->image);
        $deleted = Storage::disk('public')->delete($file_name);
        if($deleted){
            $fabricPattern->image = null;
            $fabricPattern->save();
        }
        return response()->json([
            'status' => (bool) $deleted,
            'data'   => $file_name,
            'message' => $deleted ? 'Image Deleted!' : 'Error Deleting Image'
        ]);
    }  

    public function destroy(FabricPattern $fabricPattern)
    {
        $status = $fabricPattern->delete();

        return response()->json([
            'status' => $status,
            'message' => $status ? 'Item Deleted!' : 'Error Deleting Item'
        ]);
    }

    public function stock(FabricPattern $fabricPattern) {
        return response()->json($fabricPattern->stock->sum('remaining_quantity'), 200);
    }
}
