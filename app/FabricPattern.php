<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class FabricPattern extends Model
{
    use SoftDeletes;

    public $timestamps = false;
    
    protected $fillable = [
        'sku','fabric_id','design_id','color_id','image'
    ];
    
    public function fabric(){
        return $this->belongsTo('App\Fabric')->withTrashed();
    }
    
    public function design(){
        return $this->belongsTo('App\Design')->withTrashed();
    }
    
    public function color(){
        return $this->belongsTo('App\Color')->withTrashed();
    }

    public function stock(){
        return $this->hasMany('App\Stock');
    }
}
