<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Stock extends Model
{
    public $timestamps = false;
    
    protected $fillable = [
        'fabric_pattern_id','roll_quantity','quantity','remaining_quantity',
    ];

    public function fabricPattern()
    {
        return $this->belongsTo('App\FabricPattern','fabric_pattern_id')->withTrashed();
    }
}
