<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PurchaseArticle extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'purchase_id','fabric_pattern_id','description','roll_quantity','quantity','price','total'
    ];

    public function fabricPattern()
    {
        return $this->belongsTo('App\FabricPattern','fabric_pattern_id')->withTrashed();
    }

    public function purchase()
    {
        return $this->belongsTo('App\Purchase')->withTrashed();
    }
}