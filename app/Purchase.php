<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Purchase extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'supplier_id','balance','total','user_id','currency','status'
    ];

    public function supplier()
    {
        return $this->belongsTo('App\Supplier')->withTrashed();
    }

    public function user()
    {
        return $this->belongsTo('App\User')->withTrashed();
    }

    public function articles()
    {
        return $this->hasMany('App\PurchaseArticle')->withTrashed();
    }

    public function payments()
    {
        return $this->hasMany('App\PurchasePayment');
    }
}
