<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Sale extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'customer_id','balance','total','revenue','commission','user_id','currency','status'
    ];

    public function customer()
    {
        return $this->belongsTo('App\Customer')->withTrashed();
    }
    
    public function user()
    {
        return $this->belongsTo('App\User')->withTrashed();
    }

    public function articles()
    {
        return $this->hasMany('App\SaleArticle')->withTrashed();
    }

    public function payments()
    {
        return $this->hasMany('App\SalePayment');
    }
}
