<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FabricDesign extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'fabric_id','design_id'
    ];
    
    public function fabric(){
        return $this->belongsTo('App\Fabric');
    }
    
    public function design(){
        return $this->belongsTo('App\Design');
    }
}
