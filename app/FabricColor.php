<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FabricColor extends Model
{
    public $timestamps = false;
    
    protected $fillable = [
        'fabric_id','color_id'
    ];
    
    public function fabric(){
        return $this->belongsTo('App\Fabric');
    }
    
    public function color(){
        return $this->belongsTo('App\Color');
    }
}
