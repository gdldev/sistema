<?php

namespace App\Mail;

use PDF;
use App\Purchase;
use App\NumALetra;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class PurchaseOrder extends Mailable
{
    use Queueable, SerializesModels;

    protected $purchase;
    protected $pdf;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Purchase $purchase, $pdf)
    {
        $this->purchase = $purchase;
        $this->pdf = $pdf;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('purchase_email',['data' => $this->purchase])
            ->subject('Compra '.$this->purchase->id)
            ->attachData($this->pdf, 'Compra '.$this->purchase->id.'.pdf', ['mime' => 'application/pdf',]);
    }
}
