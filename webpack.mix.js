const mix = require('laravel-mix');
const SWPrecacheWebpackPlugin = require('sw-precache-webpack-plugin');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js')
   .js('resources/js/bootstrap.js', 'public/js')
   .sass('resources/sass/app.scss', 'public/css');

mix.webpackConfig({
   plugins: [
      new SWPrecacheWebpackPlugin({
         cacheId: 'pwa',
         filename: 'service-worker.js',
         staticFileGlobs: [
            'public/**/*.{css,eot,svg,ttf,woff,woff2,js,html}',
            'public/img/**/*.*',
         ],
         minify: true,
         stripPrefix: 'public/',
         handleFetch: true,
         dynamicUrlToDependencies: {
            '/': ['resources/views/landing.blade.php'],
            // '/posts': ['resources/views/posts.blade.php']
         },
         staticFileGlobsIgnorePatterns: [/\.map$/, /mix-manifest\.json$/, /manifest\.json$/, /service-worker\.js$/],
         navigateFallback: '/',
         runtimeCaching: [
            {
               urlPattern: /^https:\/\/fonts\.googleapis\.com\//,
               handler: 'cacheFirst'
            }
         ],
         // importScripts: ['./js/push_message.js']
      })
   ]
});