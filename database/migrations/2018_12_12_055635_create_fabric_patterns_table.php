<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFabricPatternsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fabric_patterns', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('fabric_id');
            $table->unsignedInteger('design_id')->nullable();
            $table->unsignedInteger('color_id')->nullable();
            $table->string('sku')->nullable();
            $table->string('image')->nullable();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fabric_patterns');
    }
}
