<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Folio {{ $data->id }}</title>
	<link rel="stylesheet" type="text/css" href="{{ public_path('css/bootstrap.css') }}" media="all">
	<link rel="stylesheet" type="text/css" href="{{ public_path('css/factura.css') }}" media="all">
</head>
<body>
	<div class="original">
		<div>
			<div class="row">
				<div class="pull-left">
					<img src="{{ $message->embed(public_path('/img/logo-144.png')) }}" style="width: 140px;margin: 4px 16px">
				</div>
				<div class="pull-right" style="margin-right:20px;">
                    <h4>TEXTILES HERNANDEZ</h4>
                    <p>ventas@textileshernandez.com</p>
                    <p>Juan de Dios Robledo #1027</p>
                    <p>36657476</p>
				</div>
            </div>
        </div>
		<div class="container-fluid">
			<div class="row">
				<div class="col col-xs-12">
					<div class="col-heading text-uppercase">Datos de proveedor</div>
					<div class="col-content text-uppercase">
                        @if($data->supplier)
                        <p><b>{{ $data->supplier->name }}</b></p>
                        @else
                        <p><b>N/A</b></p>
                        @endif
					</div>
				</div>
				<div class="col col-xs-12">
                    <div class="col-heading">Nota de Compra</div>
                    <div class="col col-xs-5 text-right text-uppercase font-10">
                        <b>Folio</b><br>
                        <b>Fecha y Hora</b><br>
                    </div>
                    <div class="col col-xs-7 pull-right font-10">
                        <div style="margin-left: 8px">
                            <b>{{ $data->id }}</b><br>
                            {{ $data->created_at }}
                        </div>
                    </div>
				</div>
            </div>
			<div class="row">
				<table class="table main-table table-bordered">
					<thead class="font-10">
						<tr>
							<th>Cantidad</th>
							<th style="width: 205px">Articulo</th>
							<th>Valor Unitario</th>
							<th>Importe</th>
						</tr>
					</thead>
					<tbody>
						@foreach ($data->articles as $concepto)
						<tr>
							<td>{{ number_format($concepto->quantity, 2, '.', ',') }}</td>
							<td>
								@if (!is_null($concepto->fabricPattern))
									{{ $concepto->fabricPattern->fabric->name }}
									{{ $concepto->fabricPattern->sku }}
									@if(!is_null($concepto->fabricPattern->category))
									{{ $concepto->fabricPattern->category->name }}
									@endif
									@if(!is_null($concepto->fabricPattern->color))
									{{ $concepto->fabricPattern->color->name }}
									@endif
								@else
									{{ $concepto->description}}
								@endif
							</td>
							<td>${{ number_format($concepto->price, 2, '.', ',') }}</td>
							<td>${{ number_format($concepto->total, 2, '.', ',') }}</td>
						</tr>
						@endforeach
					</tbody>
				</table>
            </div>
			<div class="row">
				<div class="col col-xs-7">
					<div class="col-heading font-10">Importe con letra</div>
					<div class="col-content text-center font-10" style="border: 1px solid #000">
						{{ $data->total_amount }}
					</div>
				</div>
				<div class="col col-xs-5">
                    <div class="col col-xs-7 text-right">
                        <b>Total:</b><br>
                        @if($data->balance > 0)
                        <b>Saldo Pendiente:</b>
                        @endif
                    </div>
                    <div class="col col-xs-4 pull-right text-right">
                        <div style="margin-left: 8px">
                            <div style="border-top: 2px solid #000;border-bottom: 2px solid #000"><b>${{ number_format($data->total, 2, '.', ',') }}</b></div>
                        </div>
                        @if($data->balance > 0)
                        <div style="margin-left: 8px">
                            <div><b>${{ number_format($data->balance, 2, '.', ',') }}</b></div>
                        </div>
                        @endif
                    </div>
				</div>
            </div>
            @if(count($data->payments) > 0)
			<div class="row">
				<table class="table main-table table-bordered">
					<thead class="font-10">
						<tr>
							<th colspan="{{$data->currency == 2 ? '4':'3'}}" class="text-center">Abonos</th>
						</tr>
						<tr>
							<th>#</th>
							<th>Fecha</th>
							<th>Cantidad MXN</th>
							@if($data->currency == 2)
							<th>Cantidad USD</th>
							@endif
						</tr>
					</thead>
					<tbody>
						@foreach ($data->payments as $key => $concepto)
						<tr>
							<td>{{ $key+1 }}</td>
							<td>{{ date("d/m/Y H:i:s",strtotime($concepto->created_at)) }}</td>
							<td>${{ number_format($concepto->amount_mxn, 2, '.', ',') }} MXN</td>
							@if($data->currency == 2)
							<td>${{ number_format($concepto->amount_usd, 2, '.', ',') }} USD</td>
							@endif
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>
			@endif
        </div>
    </div>
    <div class="my-footer"><img src="{{ $message->embed(public_path('/img/logo-184.png')) }}"></div>
</body>
</html>