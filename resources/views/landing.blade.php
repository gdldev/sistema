<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <meta name="theme-color" content="#eee"/>
        <link rel="apple-touch-icon" sizes="60x60" href="/img/icons/apple-icon-60x60.png">
        <link rel="apple-touch-icon" sizes="76x76" href="/img/icons/apple-icon-76x76.png">
        <link rel="apple-touch-icon" sizes="120x120" href="/img/icons/apple-icon-120x120.png">
        <link rel="apple-touch-icon" sizes="152x152" href="/img/icons/apple-icon-152x152.png">
        <link rel="apple-touch-icon" sizes="180x180" href="/img/icons/apple-icon-180x180.png">
        <meta name="apple-mobile-web-app-capable" content="yes">
        <link rel="manifest" href="{{url('/manifest.json')}}">
        <script>
        const isLocalhost = Boolean(
            window.location.hostname === "localhost" ||
            window.location.hostname === "[::1]" ||
            window.location.hostname.match(
                /^127(?:\.(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)){3}$/
            )
        );
        window.addEventListener("load", function() {
            if (
            "serviceWorker" in navigator &&
            (window.location.protocol === "https:" || isLocalhost)
            ) {
            navigator.serviceWorker
                .register("/service-worker.js")
                .then(function(registration) {
                registration.onupdatefound = function() {
                    if (navigator.serviceWorker.controller) {
                    const installingWorker = registration.installing;
        
                    installingWorker.onstatechange = function() {
                        switch (installingWorker.state) {
                        case "installed":
                            if(confirm("Hay una actualizacion, ¿desea instalar?")){
                                location.reload()
                            };
                            break;
                        case "redundant":
                            throw new Error(
                            "The installing " + "service worker became redundant."
                            );
                        default:
                        // Ignore
                        }
                    };
                    }
                };
                })
                .catch(function(e) {
                    console.error("Error during service worker registration:", e);
                });
            }
        });
        </script>
        <title>Textiles Hernandez</title>
        <link href=" {{ mix('css/app.css') }}" rel="stylesheet">
    </head>
    <body>
        <div id="app">
            <app></app>
        </div>
        <script src="{{ mix('js/bootstrap.js') }}"></script>
        <script src="{{ mix('js/app.js') }}"></script>
    </body>
</html>
