exports.fetch = ({ method = "get", url, data = {}, params = null }) => {
    return axios({ method, url, data, params })
        .then(res => {
            return [null, res.data];
        })
        .catch(err => [err]);
}
exports.months = [
    "Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio",
    "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"
]
exports.user_types = [
    {id:1, type:"Administrador"},
    {id:2, type:"Vendedor"},
    {id:3, type:"Cliente"}
];
exports.currencies = [
    {id:1, name:'Pesos (MXN)'},
    {id:2, name:'Dolares (USD)'}
];
const availableLogic = {
    '<':' menor a {a}',
    '>':' mayor a {a}',
    '<=':' menor o igual a {a}',
    '>=':' mayor o igual a {a}',
    '=':' igual a {a}',
    '!=':' diferente a {a}',
    'not like':' diferente a {a}',
}
exports.getLogic = function(logic){
    return availableLogic[logic] || ': {a}';
}
exports.getFilterValues = function(items,{label='name', value='id'}){
    let filterValues = [];
    items.forEach(element => {
        filterValues.push({label: element[label], value:element[value]})
    });
    return filterValues;
}