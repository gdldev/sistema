import Vue from 'vue'
import Vuex from 'vuex'
import VueRouter from 'vue-router'
import 'es6-promise/auto'
import Vue2Filters from 'vue2-filters'
 
Vue.use(Vue2Filters)
Vue.use(Vuex)
Vue.use(VueRouter)

import Util from './utils';
window.Util = Util;
import App from './views/App'
import cfenv from './config/cfenv'

const router = new VueRouter({
    mode: 'history',
    routes: cfenv.getRoutes(),
})

const store = new Vuex.Store({
    state: {
        user: localStorage.getItem('app.user') ? JSON.parse(localStorage.getItem('app.user')) : {},
        isLoggedIn: localStorage.getItem("app.jwt") != null,
        menu: cfenv.getMainMenu(),
        sales: localStorage.getItem('app.sales') ? JSON.parse(localStorage.getItem('app.sales')) : [{
            item: {
                update_stock: true,
                customer_id: null,
                balance: 0,
                total: 0,
                revenue: 0,
                commission: 0,
                articles: [],
                payments: [],
                currency: 1,
                customer: { name: "" },
                fabric_pattern: { name: "" },
                fabric_pattern_id: ""
            },
            payment: { amount_mxn: null, amount_usd: null },
            currencies: Util.currencies,
            article: {
                description: null,
                quantity: null,
                price: null,
                total: 0,
                revenue: 0,
                commission: 0
            },
            fabric_patterns: [],
            customers: []
        }],
        sale: 0
    },
    mutations: {
        addSale(state) {
            state.sales.push({
                item: {
                    update_stock: true,
                    customer_id: null,
                    balance: 0,
                    total: 0,
                    revenue: 0,
                    commission: 0,
                    articles: [],
                    payments: [],
                    currency: 1,
                    customer: { name: "" },
                    fabric_pattern: { name: "" },
                    fabric_pattern_id: ""
                },
                payment: { amount_mxn: null, amount_usd: null },
                currencies: Util.currencies,
                article: {
                    description: null,
                    quantity: null,
                    price: null,
                    total: 0,
                    revenue: 0,
                    commission: 0
                },
                fabric_patterns: [],
                customers: []
            })
            localStorage.setItem('app.sales', JSON.stringify(state.sales))
        },
        selectSale(state, index) {
            state.sale = index;
        },
        deleteSale(state, index) {
            state.sales.splice(index, 1);
            state.sale = 0;
            localStorage.setItem('app.sales', JSON.stringify(state.sales))
        },
        updateSale(state) {
            localStorage.setItem('app.sales', JSON.stringify(state.sales))
        },
        updateUser(state, user) {
            localStorage.setItem('app.user', JSON.stringify(user))
            state.user = user;
        },
        login(state, { user, token }) {
            axios.defaults.headers.common["Content-Type"] = "application/json";
            axios.defaults.headers.common["Authorization"] =
                "Bearer " + token;
            localStorage.setItem('app.user', JSON.stringify(user))
            localStorage.setItem("app.jwt", token);
            state.isLoggedIn = true;
            state.user = user;
            state.menu = cfenv.getMainMenu();
        },
        logout(state) {
            state.isLoggedIn = false;
            state.user = {};
            state.sale = 0;
            state.sales = [
                {
                    item: {
                        update_stock: true,
                        customer_id: null,
                        balance: 0,
                        total: 0,
                        revenue: 0,
                        commission: 0,
                        articles: [],
                        payments: [],
                        currency: 1,
                        customer: { name: "" },
                        fabric_pattern: { name: "" },
                        fabric_pattern_id: ""
                    },
                    payment: { amount_mxn: null, amount_usd: null },
                    currencies: Util.currencies,
                    article: {
                        description: null,
                        quantity: null,
                        price: null,
                        total: 0,
                        revenue: 0,
                        commission: 0
                    },
                    fabric_patterns: [],
                    customers: []
                }
            ];
            localStorage.clear();
        }
    },
    getters: {
        hasPermission: (state) => (permission) =>
            (state.user.user_permissions.find(item => item.permission.permission == permission))
    }
})

router.beforeEach((to, from, next) => {
    if (to.matched.some(record => record.meta.requiresAuth)) {
        if (!store.state.isLoggedIn) {
            next({
                path: '/login',
                params: { nextUrl: to.fullPath }
            })
        } else {
            if (to.matched.some(record => record.meta.is_admin)) {
                if (store.state.user.user_type == 1) {
                    next();
                }
            }
            else if (to.matched.some(record => record.meta.requiredScope)) {
                if (store.getters.hasPermission(to.meta.requiredScope)) {
                    next();
                }
            }
            else next()
        }
    } else {
        next()
    }
})

const app = new Vue({
    el: '#app',
    components: { App },
    router,
    store
});