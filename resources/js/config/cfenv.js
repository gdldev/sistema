import environment from './env.json';

const router = [];

function initRouter(router, envMenu){
    envMenu.forEach(element => {
        if(element.componentPath)
            element.component = require("../"+(element.componentPath));
        if(element.children){
            element.children.forEach(children => {
                if(children.componentPath) {
                    children.component = require("../"+(children.componentPath));
                }
            });
        }
        router.push(element);
    });
}

function getMenu(envMenu){
    let menu = [];
    let user = JSON.parse(localStorage.getItem('app.user'))
    envMenu.forEach(element => {
        if(element.inMenu){
            let item = {
                name: element.inMenu.displayName,
            };
            if(element.children) {
                item.show = false;
                item.to = { href:element.inMenu.id };
                item.children = [];
                element.children.forEach(children => {
                    if(children.inMenu){
                        if(children.meta && (children.meta.requiredScope || children.meta.is_admin)) {
                            if(user && user.user_type == 1){
                                item.children.push({
                                    name: children.inMenu.displayName,
                                    to: { path: element.path +'/'+ children.path }
                                });
                            }
                            else if(user && user.user_permissions.find(item => item.permission.permission == children.meta.requiredScope)){
                                item.children.push({
                                    name: children.inMenu.displayName,
                                    to: { path: element.path +'/'+ children.path }
                                });
                            }
                        }
                        else {
                            item.children.push({
                                name: children.inMenu.displayName,
                                to: { path: element.path +'/'+ children.path }
                            });
                        }
                    }
                });
            }
            else {
                item.to = { path: element.path };
            }
            if(element.meta && (element.meta.requiredScope || element.meta.is_admin)) {
                if(user && user.user_type == 1){
                    menu.push(item);
                }
                else if(user && user.user_permissions.find(item => item.permission.permission == element.meta.requiredScope)){
                    menu.push(item);
                }
            }
            else menu.push(item);
        }
    });
    return menu;
}

initRouter(router, environment.menu);

const cfenv = {
    getByName(name){
        return environment[name] || null;
    },
    getMainMenu(){
        return getMenu(environment.menu);
    },
    getRoutes(){
        return router;
    }
}

export default cfenv;