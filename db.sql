-- MySQL dump 10.13  Distrib 5.7.23, for macos10.13 (x86_64)
--
-- Host: localhost    Database: sistema
-- ------------------------------------------------------
-- Server version	5.7.23

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `colors`
--

DROP TABLE IF EXISTS `colors`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `colors` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `colors`
--

LOCK TABLES `colors` WRITE;
/*!40000 ALTER TABLE `colors` DISABLE KEYS */;
INSERT INTO `colors` VALUES (1,'Naranja','#ff9300','2018-12-12 02:54:45','2018-12-12 02:57:00',NULL);
/*!40000 ALTER TABLE `colors` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `customers`
--

DROP TABLE IF EXISTS `customers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `customers` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(1000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `telephone` varchar(25) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mobilephone` varchar(25) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `credit_days` int(11) DEFAULT NULL,
  `comments` varchar(1000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `customers`
--

LOCK TABLES `customers` WRITE;
/*!40000 ALTER TABLE `customers` DISABLE KEYS */;
INSERT INTO `customers` VALUES (1,'Daniel Ortega','Ignacio Machain 1066',NULL,'3322565763','dani.ortega9607@gmail.com',4,'Holi','2018-12-12 21:48:46','2018-12-12 21:49:27','2018-12-12 21:49:27'),(2,'Pedro',NULL,NULL,NULL,NULL,NULL,NULL,'2018-12-14 22:13:54','2018-12-14 22:13:54',NULL),(3,'Ricardo',NULL,NULL,NULL,NULL,NULL,NULL,'2018-12-14 22:13:59','2018-12-14 22:13:59',NULL),(4,'Alan Eduardo',NULL,NULL,NULL,NULL,NULL,NULL,'2018-12-14 22:14:09','2018-12-14 22:14:09',NULL),(5,'Juan Carlos Mora',NULL,NULL,NULL,NULL,NULL,NULL,'2018-12-15 01:08:12','2018-12-15 01:08:12',NULL),(6,'Pedro Mora',NULL,NULL,NULL,NULL,NULL,NULL,'2018-12-15 01:09:40','2018-12-15 01:09:40',NULL),(7,'EL cliente',NULL,NULL,NULL,NULL,NULL,NULL,'2018-12-15 01:11:53','2018-12-15 01:11:53',NULL),(8,'Otro cliente',NULL,NULL,NULL,NULL,NULL,NULL,'2018-12-15 01:13:26','2018-12-15 01:13:26',NULL),(9,'El nuevo',NULL,NULL,NULL,NULL,NULL,NULL,'2018-12-15 01:27:50','2018-12-15 01:27:50',NULL),(10,'Luis',NULL,NULL,NULL,NULL,NULL,NULL,'2018-12-15 02:56:44','2018-12-15 02:56:44',NULL),(11,'Daniel',NULL,NULL,NULL,NULL,NULL,NULL,'2018-12-15 03:06:31','2018-12-15 03:06:31',NULL),(12,'Daniel Ortega',NULL,NULL,NULL,NULL,NULL,NULL,'2018-12-15 10:00:07','2018-12-15 10:00:07',NULL),(13,'Edgar Daniel Ortega Donjuan','Ignacio Machain 983','013322565763',NULL,'dani.ortega9607@gmail.com',NULL,NULL,'2018-12-15 11:30:08','2018-12-15 11:30:08',NULL),(14,'Luis',NULL,NULL,NULL,NULL,NULL,NULL,'2018-12-15 11:30:49','2018-12-15 11:30:49',NULL),(15,'Luis nuevo','Domicilio nuevo',NULL,NULL,'cliente@admin.com',NULL,NULL,'2018-12-15 12:27:38','2018-12-15 12:27:38',NULL),(16,'Luis nuevo','Domicilio nuevo',NULL,NULL,'cliente@admin.com',NULL,NULL,'2018-12-15 12:27:51','2018-12-15 12:27:51',NULL),(17,'Otro Cliente',NULL,NULL,NULL,'otro@admin.com',NULL,NULL,'2018-12-15 12:33:51','2018-12-15 12:33:51',NULL),(18,'Nuevo',NULL,NULL,NULL,'nuevo@admin.com',NULL,NULL,'2018-12-15 12:34:38','2018-12-15 12:34:38',NULL),(19,'Jorge',NULL,NULL,NULL,'jorge@admin.com',NULL,NULL,'2018-12-15 12:35:23','2018-12-15 12:35:23',NULL),(20,'Juan Oswaldo',NULL,NULL,NULL,'oswa@admin.com',NULL,NULL,'2018-12-16 05:26:03','2018-12-16 05:26:03',NULL);
/*!40000 ALTER TABLE `customers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `designs`
--

DROP TABLE IF EXISTS `designs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `designs` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `designs`
--

LOCK TABLES `designs` WRITE;
/*!40000 ALTER TABLE `designs` DISABLE KEYS */;
/*!40000 ALTER TABLE `designs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fabric_colors`
--

DROP TABLE IF EXISTS `fabric_colors`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fabric_colors` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fabric_id` int(10) unsigned NOT NULL,
  `color_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fabric_colors`
--

LOCK TABLES `fabric_colors` WRITE;
/*!40000 ALTER TABLE `fabric_colors` DISABLE KEYS */;
/*!40000 ALTER TABLE `fabric_colors` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fabric_designs`
--

DROP TABLE IF EXISTS `fabric_designs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fabric_designs` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fabric_id` int(10) unsigned NOT NULL,
  `design_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fabric_designs`
--

LOCK TABLES `fabric_designs` WRITE;
/*!40000 ALTER TABLE `fabric_designs` DISABLE KEYS */;
/*!40000 ALTER TABLE `fabric_designs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fabric_patterns`
--

DROP TABLE IF EXISTS `fabric_patterns`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fabric_patterns` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fabric_id` int(10) unsigned NOT NULL,
  `design_id` int(10) unsigned DEFAULT NULL,
  `color_id` int(10) unsigned DEFAULT NULL,
  `sku` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fabric_patterns`
--

LOCK TABLES `fabric_patterns` WRITE;
/*!40000 ALTER TABLE `fabric_patterns` DISABLE KEYS */;
INSERT INTO `fabric_patterns` VALUES (1,1,NULL,1,'12345','\\storage\\fabrics/0WHyJBxhGp8v4cXJh3YW1VmWeFSLzMCGM6CwkWmy.png',NULL);
/*!40000 ALTER TABLE `fabric_patterns` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fabrics`
--

DROP TABLE IF EXISTS `fabrics`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fabrics` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `buy_price` decimal(10,2) DEFAULT NULL,
  `sell_price` decimal(10,2) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fabrics`
--

LOCK TABLES `fabrics` WRITE;
/*!40000 ALTER TABLE `fabrics` DISABLE KEYS */;
INSERT INTO `fabrics` VALUES (1,'Brush',10.00,30.00,'2018-12-12 22:14:41','2018-12-15 10:00:28',NULL);
/*!40000 ALTER TABLE `fabrics` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES (1,'2014_10_12_000000_create_users_table',1),(2,'2014_10_12_100000_create_password_resets_table',1),(3,'2016_06_01_000001_create_oauth_auth_codes_table',1),(4,'2016_06_01_000002_create_oauth_access_tokens_table',1),(5,'2016_06_01_000003_create_oauth_refresh_tokens_table',1),(6,'2016_06_01_000004_create_oauth_clients_table',1),(7,'2016_06_01_000005_create_oauth_personal_access_clients_table',1),(8,'2018_12_11_170230_create_permissions_table',2),(9,'2018_12_11_170244_create_user_permissions_table',2),(10,'2018_12_11_182725_create_colors_table',3),(11,'2018_12_12_035816_create_designs_table',4),(12,'2018_12_12_042010_create_fabrics_table',4),(13,'2018_12_12_043726_create_fabric_colors_table',4),(14,'2018_12_12_043738_create_fabric_designs_table',4),(15,'2018_12_12_055635_create_fabric_patterns_table',4),(16,'2018_12_12_151229_create_customers_table',5),(17,'2018_12_12_151308_create_suppliers_table',5),(18,'2018_12_12_155359_create_stocks_table',6),(21,'2018_12_12_181601_create_purchases_table',7),(24,'2018_12_12_220829_create_purchase_articles_table',8),(25,'2018_12_14_165445_create_sale_payments_table',9),(26,'2018_12_14_165458_create_purchase_payments_table',9),(29,'2018_12_12_182005_create_sales_table',10),(30,'2018_12_12_220818_create_sale_articles_table',10);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oauth_access_tokens`
--

DROP TABLE IF EXISTS `oauth_access_tokens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `client_id` int(11) NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_access_tokens_user_id_index` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth_access_tokens`
--

LOCK TABLES `oauth_access_tokens` WRITE;
/*!40000 ALTER TABLE `oauth_access_tokens` DISABLE KEYS */;
INSERT INTO `oauth_access_tokens` VALUES ('0517b2a4aaa22c98f6ce94352d763a316e15cfebca756b4f539a778e10649d57e25c0424a2972955',1,1,'app','[\"sale-management\",\"stock-management\",\"purchase-management\",\"general-management\"]',0,'2018-12-11 23:54:21','2018-12-11 23:54:21','2019-12-11 17:54:21'),('0d07dacbb58ee1fb57e38a171634b8665743ac1b6f60dbe1aeb4ca129977a44b7644772de4c1012e',2,1,'app','[\"sale-management\",\"stock-management\"]',0,'2018-12-12 04:14:23','2018-12-12 04:14:23','2019-12-11 22:14:23'),('0faf8a7d6bb44670db71ac00e3c8fe5bf72fec118b3093441779374f99ea81e3df3350bc2b954197',2,1,'app','[\"sale-management\",\"stock-management\"]',0,'2018-12-11 23:56:45','2018-12-11 23:56:45','2019-12-11 17:56:45'),('1af1b6b79332839f6ef5d8bf1e5037854438af91b720c141fe28f91ce928a63b394518b80b85757f',2,1,'app','[\"sale-management\"]',0,'2018-12-15 11:16:20','2018-12-15 11:16:20','2019-12-15 05:16:20'),('1de26a57fb083e5654fdf5b19cfd5d531ff602ec1ca2a3be78b2cccf589b7d0b003d78bd6da2bb3b',2,1,'app','[\"sale-management\",\"stock-management\"]',0,'2018-12-12 05:01:58','2018-12-12 05:01:58','2019-12-11 23:01:58'),('1eddac7cb6a2998cb282ab8d3e1f1f9b099f451b9541ccaab54f08cbeb802b06d9c705a8b310615c',1,1,'app','[\"sale-management\",\"stock-management\",\"purchase-management\",\"general-management\"]',0,'2018-12-11 23:28:20','2018-12-11 23:28:20','2019-12-11 17:28:20'),('25fc9ed69c974fbd23f8d1a9c0658e84a641e97ea03980a3e6dbea4f370739a2ae33cb6423e0f8c9',2,1,'app','[\"sale-management\",\"stock-management\"]',0,'2018-12-15 04:45:46','2018-12-15 04:45:46','2019-12-14 22:45:46'),('2742eac2c3eac0bf7fcd1a5cf1e0fc767522f8264c8f0b4def2dfa8c85f6abcafb638ec8d71cbf23',1,1,'app','[\"sale-management\",\"stock-management\",\"purchase-management\",\"general-management\"]',0,'2018-12-11 23:56:55','2018-12-11 23:56:55','2019-12-11 17:56:55'),('2889c6dd448ece2392501d02512a61a2648ec7b35e54bcf9d2c2253004a33da5ee79cc20ff1dc3a1',1,1,'app','[\"sale-management\",\"stock-management\",\"purchase-management\",\"general-management\"]',0,'2018-12-12 21:46:36','2018-12-12 21:46:36','2019-12-12 15:46:36'),('2af191e4070ff5471e21f05ffbf5db7c26f10558eaa7f6e853a30cfb9d1168116a40eff68abf1f86',1,1,'app','[\"sale-management\",\"stock-management\",\"purchase-management\",\"general-management\"]',0,'2018-12-11 23:52:52','2018-12-11 23:52:52','2019-12-11 17:52:52'),('2d1c01485ee70f9a3036f80196786718ddadd351bd23a63e9dab5f31a4c58a03f23d40a4b7b5f566',2,1,'app','[]',0,'2018-12-15 10:43:57','2018-12-15 10:43:57','2019-12-15 04:43:57'),('365eea1048aa0701e383dfccd939cf2503819c5bb77c0d227a70792ffd55d0fd0d599da3c7ef75cc',1,1,'app','[\"sale-management\",\"stock-management\",\"purchase-management\",\"general-management\"]',0,'2018-12-12 00:17:10','2018-12-12 00:17:10','2019-12-11 18:17:10'),('3d55853fd9eff4c87832f2d833ff96eaf73fa813b60b1d109c8e216967dd949ad7d554271623566a',1,1,'app','[\"sale-management\",\"stock-management\",\"purchase-management\",\"general-management\"]',0,'2018-12-16 05:27:14','2018-12-16 05:27:14','2019-12-15 23:27:14'),('3db7365e3626421440c6d2f50290a217792989b49ad7a8e6e930d11b40dec11140f4d6884084c9fe',1,1,'app','[\"sale-management\",\"stock-management\",\"purchase-management\",\"general-management\"]',0,'2018-12-12 04:13:32','2018-12-12 04:13:32','2019-12-11 22:13:32'),('41077daae185be3536bec73399974c9b3db8c20ca916e8c841bdb37f178372aba7c8e06afaadbe55',1,1,'app','[\"sale-management\",\"stock-management\",\"purchase-management\",\"general-management\"]',0,'2018-12-15 11:52:07','2018-12-15 11:52:07','2019-12-15 05:52:07'),('42619137a6fbfad6add817f45d49a020cec6469a2fbd4b2e477d9bbde10eac9bb91d11771acd0ff0',1,1,'app','[\"sale-management\",\"stock-management\",\"purchase-management\",\"general-management\"]',0,'2018-12-16 05:25:23','2018-12-16 05:25:23','2019-12-15 23:25:23'),('51c993bed5561c242406cb5e61cf374fe2b8458ebacc8e0a14370d1a49400f8b5cd06f30ee8b1768',2,1,'app','[\"general-management\"]',0,'2018-12-12 03:12:58','2018-12-12 03:12:58','2019-12-11 21:12:58'),('56d5183cfb6499f289d72962275a0688be63e25efe3074596aa899c6f694f66aff275978a8032fc6',2,1,'app','[\"sale-management\"]',0,'2018-12-16 04:48:31','2018-12-16 04:48:31','2019-12-15 22:48:31'),('5804b53482973226747918c0b11d1de09527b9201e242edc6ef33ba32b07925510c212cbf1d290fd',2,1,'app','[\"sale-management\",\"stock-management\"]',0,'2018-12-12 04:22:42','2018-12-12 04:22:42','2019-12-11 22:22:42'),('58f6b30d54ed6d0bb7c79a91dfc117d14e717058d21b181bfe35c9ab83146b023576042531ae3c5f',2,1,'app','[\"sale-management\"]',0,'2018-12-15 11:19:11','2018-12-15 11:19:11','2019-12-15 05:19:11'),('5c36e9186852629c95a5fc63dba3d05aa68cd80d1889a364f280221c0bb5120e748e04bad8cc9b59',1,1,'app','[\"sale-management\",\"stock-management\",\"purchase-management\",\"general-management\"]',0,'2018-12-11 23:51:36','2018-12-11 23:51:36','2019-12-11 17:51:36'),('6c934f59cf6eb12e1ea2b0959005e893300e589a666b2795155d1100aebe8b4dd8d8288b2d76b19a',1,1,'app','[\"sale-management\",\"stock-management\",\"purchase-management\",\"general-management\"]',0,'2018-12-16 05:26:13','2018-12-16 05:26:13','2019-12-15 23:26:13'),('779442a5cadf06f556da0f279864dd2db7b6e553552bdf0e65c5b8d3e9ed200afb28e1262105eb2f',2,1,'app','[\"sale-management\",\"stock-management\"]',0,'2018-12-12 03:54:10','2018-12-12 03:54:10','2019-12-11 21:54:10'),('7d4221374883a07ef5ad59eb5b693588ce360efe80c1675c9af6f861f1c0a30edc8bb9243c999f94',2,1,'app','[\"general-management\"]',0,'2018-12-12 03:52:15','2018-12-12 03:52:15','2019-12-11 21:52:15'),('7f9dcd87e61f643d2dccf9aae25cc6580fbe3e433b4b357b4200868509f740489219b87ef3ab3fc8',2,1,'app','[\"sale-management\",\"stock-management\"]',0,'2018-12-11 23:54:10','2018-12-11 23:54:10','2019-12-11 17:54:10'),('8799eed700c9102cf29b4f5521dbd8b5fd0662e353e8294a4360b3b2c0ef55960231936b701eedde',1,1,'app','[\"sale-management\",\"stock-management\",\"purchase-management\",\"general-management\"]',0,'2018-12-15 09:54:10','2018-12-15 09:54:10','2019-12-15 03:54:10'),('8a609a2ae54c067301ebed91eed6586c91b29af6afdeb30195be80a55599102ea42c2afcfd47b123',1,1,'app','[\"sale-management\",\"stock-management\",\"purchase-management\",\"general-management\"]',0,'2018-12-15 11:15:33','2018-12-15 11:15:33','2019-12-15 05:15:33'),('8af78ea8e16e91001951ac8065ef478529d7e28456d90071d96af727f91576c98e93d3d598e2bd48',2,1,'app','[\"sale-management\",\"stock-management\"]',0,'2018-12-12 04:31:42','2018-12-12 04:31:42','2019-12-11 22:31:42'),('8af97b133c468015b97dfdb2f4d11bd56cc95f2473a9ecc1f56ac7c94bc52def0fa2c1c50b959214',1,1,'app','[]',0,'2018-12-11 23:22:12','2018-12-11 23:22:12','2019-12-11 17:22:12'),('92b4d8629eebf74828c05abd387255ac4893983f1d9f09dbf70c811201f98b758df39006db392b7a',1,1,'app','[\"sale-management\",\"stock-management\",\"purchase-management\",\"general-management\"]',0,'2018-12-11 23:53:16','2018-12-11 23:53:16','2019-12-11 17:53:16'),('9403e0affa05f31fd4e241050b6ff676b5098870ce24fa9c0730a9b845c03822473c7412f9cb227d',1,1,'app','[\"sale-management\",\"stock-management\",\"purchase-management\",\"general-management\"]',0,'2018-12-12 04:16:24','2018-12-12 04:16:24','2019-12-11 22:16:24'),('94ad2a2a54e043cc203dadc95d02babd09964df191500fa8fd8456afac6242e3ac880453b4e4be0e',1,1,'app','[\"sale-management\",\"stock-management\",\"purchase-management\",\"general-management\"]',0,'2018-12-15 11:29:37','2018-12-15 11:29:37','2019-12-15 05:29:37'),('a5cc8eadc84c6526cd9c0918ab2b73afe982533fda5de8f64f8e6af1efec13726a894ec64cfd972f',1,1,'app','[\"sale-management\",\"stock-management\",\"purchase-management\",\"general-management\"]',0,'2018-12-12 00:21:37','2018-12-12 00:21:37','2019-12-11 18:21:37'),('a6826f762b380a86d5a3ba600e00620c96053b6f7dfc4b557944317060a981ea2453ca09f566d206',2,1,'app','[\"general-management\"]',0,'2018-12-12 03:12:06','2018-12-12 03:12:06','2019-12-11 21:12:06'),('ab4ffa42237bb68b9233855f5622f892382c73f4c653c4302834e737d4f37ac4e455538e22cb7bff',1,1,'app','[\"sale-management\",\"stock-management\",\"purchase-management\",\"general-management\"]',0,'2018-12-16 05:12:45','2018-12-16 05:12:45','2019-12-15 23:12:45'),('ace0fce3da70c59c194c5d54e3d9aad9eb44a524951db30de9c80852bf1a0da24629362cb541e797',1,1,'app','[\"sale-management\",\"stock-management\",\"purchase-management\",\"general-management\"]',0,'2018-12-11 23:35:03','2018-12-11 23:35:03','2019-12-11 17:35:03'),('ae48035ed12451e4200a54d777d87302894a46525bff1af94cc6aaa4771675c2b0368018adca544d',1,1,'app','[\"sale-management\",\"stock-management\",\"purchase-management\",\"general-management\"]',0,'2018-12-12 03:34:14','2018-12-12 03:34:14','2019-12-11 21:34:14'),('b2db96600436c555313773827ac7751cb82cf6579002e0ad620d9dcfbb86dce9a7c39da066aa8c81',1,1,'app','[\"sale-management\",\"stock-management\",\"purchase-management\",\"general-management\"]',0,'2018-12-12 04:31:53','2018-12-12 04:31:53','2019-12-11 22:31:53'),('b52267df9fdaf76756d6b175ec1798abd74e9afb36580f62d2a1f33a7d7cb67efb339a5b3f394ced',2,1,'app','[\"sale-management\",\"stock-management\"]',0,'2018-12-12 00:20:52','2018-12-12 00:20:52','2019-12-11 18:20:52'),('bf2573ad3b4ce0f0657a10966acc2097619c5309445b7c10c03feae1f615c9c2dbb1e939bbaf37b2',1,1,'app','[\"sale-management\",\"stock-management\",\"purchase-management\",\"general-management\"]',0,'2018-12-16 05:22:55','2018-12-16 05:22:55','2019-12-15 23:22:55'),('c0f25aca8b64f9382cd888817bd06e8a7f50f7de6b786be32423088b758c6c7eb2eb509d542f312e',2,1,'app','[\"sale-management\",\"stock-management\"]',0,'2018-12-11 23:51:16','2018-12-11 23:51:16','2019-12-11 17:51:16'),('c5073391f98768c14c9389819189896b0609dd632733ac7e5fb8f43a3068b03a191c6d69900dd3c6',1,1,'app','[]',0,'2018-12-11 21:58:53','2018-12-11 21:58:53','2019-12-11 15:58:53'),('cb36e3826fdbb5007f0e3af7fe2d93a162b634211dd525aa18bc5e43a5599f8a231ab232a8bb12e1',1,1,'app','[\"sale-management\",\"stock-management\",\"purchase-management\",\"general-management\"]',0,'2018-12-12 03:53:07','2018-12-12 03:53:07','2019-12-11 21:53:07'),('dba6ba412c5e8b1ee8236a49377e7d2decf8076364b7bd7abd131959326f983f4b6877b57a964d28',2,1,'app','[\"sale-management\",\"stock-management\"]',0,'2018-12-12 00:21:24','2018-12-12 00:21:24','2019-12-11 18:21:24'),('de4dd3a52fe0cae79fa1124871481ff0ab7afafc960615c19915bda481d39ced6a20fade33e947b9',1,1,'app','[\"sale-management\",\"stock-management\",\"purchase-management\",\"general-management\"]',0,'2018-12-12 03:10:16','2018-12-12 03:10:16','2019-12-11 21:10:16'),('e1f90b50386bea8f004834b3634d4b07d6ecaf881369093d34c695fab81f3049c04fbdaacd6476c2',1,1,'app','[\"sale-management\",\"stock-management\",\"purchase-management\",\"general-management\"]',0,'2018-12-15 12:33:30','2018-12-15 12:33:30','2019-12-15 06:33:30'),('e938de46118a4ce428e80a995c6a0c56329e2e871559eb67ecad827cd72dc59c4af6c2618d0c706c',2,1,'app','[\"sale-management\",\"stock-management\"]',0,'2018-12-12 03:10:38','2018-12-12 03:10:38','2019-12-11 21:10:38'),('ea6f86e01d6f3d5bb450eb07e004707dd4dcc38cf82382c29fbc9252d4f171c62d44cd9ac08be741',2,1,'app','[\"sale-management\"]',0,'2018-12-16 05:19:58','2018-12-16 05:19:58','2019-12-15 23:19:58'),('ef670c3c8e8b21e4faaf6e8e9aff8a7c8dfc8952fee28ccf5af1ab53be50c0d5ad2b60eba07c39dc',1,1,'app','[\"sale-management\",\"stock-management\",\"purchase-management\",\"general-management\"]',0,'2018-12-11 23:32:56','2018-12-11 23:32:56','2019-12-11 17:32:56'),('f42a61a228027f2371000581ba6ff8dfa16aaa2bcb801087e1cc171c9ded4105c60361d62d2796de',3,1,'app','[]',0,'2018-12-15 12:28:19','2018-12-15 12:28:19','2019-12-15 06:28:19'),('f9489cd6d4bca64fda8bfd92566256f05b4f3b77aac9287d45d9d90321bfc0ea747aff40b82126c3',7,1,'app','[]',0,'2018-12-16 05:26:32','2018-12-16 05:26:32','2019-12-15 23:26:32'),('fe8083000bbba4dfbc7a66aa64a4e9772efb07b72c9fa041d573ed08ea0c7624f62f0a1ba6adf198',1,1,'app','[\"sale-management\",\"stock-management\",\"purchase-management\",\"general-management\"]',0,'2018-12-12 03:10:50','2018-12-12 03:10:50','2019-12-11 21:10:50');
/*!40000 ALTER TABLE `oauth_access_tokens` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oauth_auth_codes`
--

DROP TABLE IF EXISTS `oauth_auth_codes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `client_id` int(11) NOT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth_auth_codes`
--

LOCK TABLES `oauth_auth_codes` WRITE;
/*!40000 ALTER TABLE `oauth_auth_codes` DISABLE KEYS */;
/*!40000 ALTER TABLE `oauth_auth_codes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oauth_clients`
--

DROP TABLE IF EXISTS `oauth_clients`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oauth_clients` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `redirect` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_clients_user_id_index` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth_clients`
--

LOCK TABLES `oauth_clients` WRITE;
/*!40000 ALTER TABLE `oauth_clients` DISABLE KEYS */;
INSERT INTO `oauth_clients` VALUES (1,NULL,'Laravel Personal Access Client','L9DdLrtAw2jhpEDlqZfjqYLWI8DOZvWTrNDK1eRE','http://localhost',1,0,0,'2018-12-11 21:01:25','2018-12-11 21:01:25'),(2,NULL,'Laravel Password Grant Client','XtISqZTV3d0IXFMF3ZG7Dq4Uy2lyalIjjaMFeapj','http://localhost',0,1,0,'2018-12-11 21:01:25','2018-12-11 21:01:25');
/*!40000 ALTER TABLE `oauth_clients` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oauth_personal_access_clients`
--

DROP TABLE IF EXISTS `oauth_personal_access_clients`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oauth_personal_access_clients` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `client_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_personal_access_clients_client_id_index` (`client_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth_personal_access_clients`
--

LOCK TABLES `oauth_personal_access_clients` WRITE;
/*!40000 ALTER TABLE `oauth_personal_access_clients` DISABLE KEYS */;
INSERT INTO `oauth_personal_access_clients` VALUES (1,1,'2018-12-11 21:01:25','2018-12-11 21:01:25');
/*!40000 ALTER TABLE `oauth_personal_access_clients` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oauth_refresh_tokens`
--

DROP TABLE IF EXISTS `oauth_refresh_tokens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_refresh_tokens_access_token_id_index` (`access_token_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth_refresh_tokens`
--

LOCK TABLES `oauth_refresh_tokens` WRITE;
/*!40000 ALTER TABLE `oauth_refresh_tokens` DISABLE KEYS */;
/*!40000 ALTER TABLE `oauth_refresh_tokens` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `permissions`
--

DROP TABLE IF EXISTS `permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `permissions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `permission` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `permissions`
--

LOCK TABLES `permissions` WRITE;
/*!40000 ALTER TABLE `permissions` DISABLE KEYS */;
INSERT INTO `permissions` VALUES (1,'sale-management','Gestion de Ventas'),(2,'stock-management','Gestion de Almacen'),(3,'purchase-management','Gestion de Compras'),(4,'general-management','Gestion General');
/*!40000 ALTER TABLE `permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `purchase_articles`
--

DROP TABLE IF EXISTS `purchase_articles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `purchase_articles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `purchase_id` int(10) unsigned NOT NULL,
  `fabric_pattern_id` int(10) unsigned DEFAULT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `roll_quantity` int(11) DEFAULT NULL,
  `quantity` decimal(10,2) NOT NULL,
  `price` decimal(10,2) NOT NULL,
  `total` decimal(10,2) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `purchase_articles`
--

LOCK TABLES `purchase_articles` WRITE;
/*!40000 ALTER TABLE `purchase_articles` DISABLE KEYS */;
INSERT INTO `purchase_articles` VALUES (1,1,1,NULL,5,200.00,24.00,4800.00,'2018-12-14 02:58:56','2018-12-14 03:29:13','2018-12-14 03:29:13'),(2,1,1,'Algo mas',NULL,20.00,300.00,6000.00,'2018-12-14 02:58:56','2018-12-14 03:30:23','2018-12-14 03:30:23'),(3,2,1,NULL,4,20.00,10.00,200.00,'2018-12-14 03:15:19','2018-12-14 03:19:51','2018-12-14 03:19:51'),(4,4,1,NULL,10,200.00,20.00,4000.00,'2018-12-14 03:38:15','2018-12-14 03:38:15',NULL),(5,5,1,NULL,10,200.00,20.00,4000.00,'2018-12-14 03:38:35','2018-12-14 03:38:35',NULL),(6,6,1,NULL,10,200.00,20.00,4000.00,'2018-12-14 03:38:47','2018-12-14 03:38:47',NULL),(7,7,1,NULL,3,300.00,20.00,6000.00,'2018-12-14 03:40:42','2018-12-14 03:40:42',NULL),(8,8,1,NULL,1,200.00,20.00,4000.00,'2018-12-14 03:42:03','2018-12-14 03:42:03',NULL),(9,9,NULL,'algo que no',NULL,2.00,200.00,400.00,'2018-12-14 03:43:11','2018-12-14 03:43:11',NULL),(10,10,NULL,'algo que no',NULL,2.00,200.00,400.00,'2018-12-14 03:43:54','2018-12-14 03:43:54',NULL),(11,11,1,NULL,20,200.00,20.00,4000.00,'2018-12-14 03:51:59','2018-12-14 03:51:59',NULL),(12,11,NULL,'Algo que no',NULL,20.00,20.00,400.00,'2018-12-14 03:51:59','2018-12-14 03:51:59',NULL),(13,11,1,NULL,4,50.00,30.00,1500.00,'2018-12-14 03:51:59','2018-12-14 03:51:59',NULL),(14,12,1,NULL,10,200.00,50.00,10000.00,'2018-12-14 23:57:29','2018-12-14 23:57:29',NULL),(15,13,1,NULL,10,200.00,50.00,10000.00,'2018-12-14 23:57:43','2018-12-14 23:57:43',NULL),(16,14,1,NULL,10,200.00,50.00,10000.00,'2018-12-14 23:59:36','2018-12-14 23:59:36',NULL),(17,15,1,NULL,10,30.00,30.00,900.00,'2018-12-15 00:46:52','2018-12-15 00:49:52','2018-12-15 00:49:52'),(18,15,1,NULL,10,20.00,23.00,460.00,'2018-12-15 00:46:52','2018-12-15 00:49:43','2018-12-15 00:49:43'),(19,16,1,NULL,10,10.00,30.00,300.00,'2018-12-15 00:51:31','2018-12-15 00:51:43','2018-12-15 00:51:43'),(20,16,NULL,'Algo que vendo',NULL,10.00,20.00,200.00,'2018-12-15 00:51:31','2018-12-15 00:51:44','2018-12-15 00:51:44'),(21,18,1,NULL,50,200000.00,20.00,4000000.00,'2018-12-15 03:03:02','2018-12-15 03:03:02',NULL);
/*!40000 ALTER TABLE `purchase_articles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `purchase_payments`
--

DROP TABLE IF EXISTS `purchase_payments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `purchase_payments` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `purchase_id` int(10) unsigned NOT NULL,
  `amount_mxn` decimal(10,2) NOT NULL,
  `amount_usd` decimal(10,2) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `purchase_payments`
--

LOCK TABLES `purchase_payments` WRITE;
/*!40000 ALTER TABLE `purchase_payments` DISABLE KEYS */;
INSERT INTO `purchase_payments` VALUES (1,14,200.00,NULL,'2018-12-14 23:59:36','2018-12-14 23:59:36',NULL),(2,14,2000.00,NULL,'2018-12-14 23:59:36','2018-12-14 23:59:36',NULL),(3,15,6000.00,300.00,'2018-12-15 00:46:52','2018-12-15 00:46:52',NULL),(4,16,100.00,NULL,'2018-12-15 00:51:31','2018-12-15 00:51:50','2018-12-15 00:51:50'),(5,16,100.00,NULL,'2018-12-15 00:52:05','2018-12-15 00:52:23','2018-12-15 00:52:23');
/*!40000 ALTER TABLE `purchase_payments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `purchases`
--

DROP TABLE IF EXISTS `purchases`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `purchases` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `supplier_id` int(10) unsigned DEFAULT NULL,
  `balance` decimal(10,2) NOT NULL,
  `total` decimal(10,2) NOT NULL,
  `currency` tinyint(4) NOT NULL DEFAULT '1',
  `user_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `purchases`
--

LOCK TABLES `purchases` WRITE;
/*!40000 ALTER TABLE `purchases` DISABLE KEYS */;
INSERT INTO `purchases` VALUES (1,NULL,0.00,0.00,1,1,'2018-12-14 02:58:56','2018-12-14 03:30:24',NULL),(2,2,0.00,0.00,1,1,'2018-12-14 03:15:19','2018-12-14 03:53:34','2018-12-14 03:53:34'),(3,NULL,0.00,0.00,1,1,'2018-12-14 03:32:52','2018-12-14 03:53:56','2018-12-14 03:53:56'),(4,NULL,4000.00,4000.00,1,1,'2018-12-14 03:38:15','2018-12-14 21:24:49','2018-12-14 21:24:49'),(5,NULL,4000.00,4000.00,1,1,'2018-12-14 03:38:35','2018-12-14 21:25:02','2018-12-14 21:25:02'),(6,NULL,4000.00,4000.00,1,1,'2018-12-14 03:38:47','2018-12-14 03:38:47',NULL),(7,NULL,6000.00,6000.00,1,1,'2018-12-14 03:40:42','2018-12-14 03:40:42',NULL),(8,2,4000.00,4000.00,2,1,'2018-12-14 03:42:03','2018-12-14 03:42:03',NULL),(9,NULL,400.00,400.00,2,1,'2018-12-14 03:43:11','2018-12-14 03:43:11',NULL),(10,NULL,400.00,400.00,2,1,'2018-12-14 03:43:54','2018-12-14 03:43:54',NULL),(11,NULL,5900.00,5900.00,1,1,'2018-12-14 03:51:59','2018-12-14 03:51:59',NULL),(12,NULL,7800.00,10000.00,1,1,'2018-12-14 23:57:29','2018-12-14 23:57:29',NULL),(13,NULL,7800.00,10000.00,1,1,'2018-12-14 23:57:43','2018-12-14 23:57:43',NULL),(14,NULL,5600.00,10000.00,1,1,'2018-12-14 23:59:36','2018-12-14 23:59:36',NULL),(15,NULL,0.00,0.00,2,1,'2018-12-15 00:46:52','2018-12-15 00:49:52',NULL),(16,NULL,0.00,0.00,1,1,'2018-12-15 00:51:31','2018-12-15 00:52:23',NULL),(17,3,0.00,0.00,1,1,'2018-12-15 01:13:40','2018-12-15 01:13:40',NULL),(18,NULL,4000000.00,4000000.00,1,1,'2018-12-15 03:03:02','2018-12-15 03:03:02',NULL);
/*!40000 ALTER TABLE `purchases` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sale_articles`
--

DROP TABLE IF EXISTS `sale_articles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sale_articles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `sale_id` int(10) unsigned NOT NULL,
  `fabric_pattern_id` int(10) unsigned DEFAULT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `quantity` decimal(10,2) NOT NULL,
  `price` decimal(10,2) NOT NULL,
  `total` decimal(10,2) NOT NULL,
  `revenue` decimal(10,2) NOT NULL,
  `commission` decimal(10,2) NOT NULL,
  `cancelled_by` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sale_articles`
--

LOCK TABLES `sale_articles` WRITE;
/*!40000 ALTER TABLE `sale_articles` DISABLE KEYS */;
INSERT INTO `sale_articles` VALUES (1,1,1,NULL,10.00,30.00,300.00,200.00,0.00,NULL,'2018-12-16 04:46:54','2018-12-16 04:46:54',NULL),(2,2,1,NULL,10.00,30.00,300.00,200.00,9.00,NULL,'2018-12-16 05:10:36','2018-12-16 05:10:36',NULL);
/*!40000 ALTER TABLE `sale_articles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sale_payments`
--

DROP TABLE IF EXISTS `sale_payments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sale_payments` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `sale_id` int(10) unsigned NOT NULL,
  `amount_mxn` decimal(10,2) NOT NULL,
  `amount_usd` decimal(10,2) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sale_payments`
--

LOCK TABLES `sale_payments` WRITE;
/*!40000 ALTER TABLE `sale_payments` DISABLE KEYS */;
INSERT INTO `sale_payments` VALUES (1,4,100.00,NULL,'2018-12-15 00:10:58','2018-12-15 00:16:28','2018-12-15 00:16:28'),(2,4,100.00,NULL,'2018-12-15 00:17:00','2018-12-15 00:17:09','2018-12-15 00:17:09'),(3,4,65.00,NULL,'2018-12-15 00:20:27','2018-12-15 00:20:35','2018-12-15 00:20:35'),(4,11,200.00,NULL,'2018-12-15 02:56:44','2018-12-15 02:56:44',NULL),(5,6,5.00,NULL,'2018-12-15 11:18:44','2018-12-15 11:18:44',NULL);
/*!40000 ALTER TABLE `sale_payments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sales`
--

DROP TABLE IF EXISTS `sales`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sales` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `customer_id` int(10) unsigned DEFAULT NULL,
  `balance` decimal(10,2) NOT NULL DEFAULT '0.00',
  `total` decimal(10,2) NOT NULL DEFAULT '0.00',
  `revenue` decimal(10,2) NOT NULL DEFAULT '0.00',
  `commission` decimal(10,2) NOT NULL DEFAULT '0.00',
  `currency` tinyint(4) NOT NULL DEFAULT '1',
  `user_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sales`
--

LOCK TABLES `sales` WRITE;
/*!40000 ALTER TABLE `sales` DISABLE KEYS */;
INSERT INTO `sales` VALUES (1,20,300.00,300.00,200.00,0.00,1,1,'2018-12-16 04:46:54','2018-12-16 05:26:23',NULL),(2,NULL,300.00,300.00,200.00,9.00,1,2,'2018-12-16 05:10:36','2018-12-16 05:10:36',NULL);
/*!40000 ALTER TABLE `sales` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stocks`
--

DROP TABLE IF EXISTS `stocks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stocks` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fabric_pattern_id` int(10) unsigned NOT NULL,
  `roll_quantity` int(11) NOT NULL,
  `quantity` decimal(10,2) NOT NULL,
  `remaining_quantity` decimal(10,2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stocks`
--

LOCK TABLES `stocks` WRITE;
/*!40000 ALTER TABLE `stocks` DISABLE KEYS */;
INSERT INTO `stocks` VALUES (2,1,10,200.00,0.00),(3,1,3,300.00,0.00),(4,1,20,200.00,0.00),(5,1,4,50.00,0.00),(6,1,10,30.00,0.00),(7,1,10,20.00,0.00),(8,1,50,200000.00,199498.00);
/*!40000 ALTER TABLE `stocks` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `suppliers`
--

DROP TABLE IF EXISTS `suppliers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `suppliers` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(1000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `telephone` varchar(25) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mobilephone` varchar(25) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `comments` varchar(1000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `suppliers`
--

LOCK TABLES `suppliers` WRITE;
/*!40000 ALTER TABLE `suppliers` DISABLE KEYS */;
INSERT INTO `suppliers` VALUES (1,'Proveedor 1',NULL,'013322565763',NULL,'dani.ortega9607@gmail.com',NULL,'2018-12-12 21:50:34','2018-12-12 21:51:02','2018-12-12 21:51:02'),(2,'Edgar Daniel Ortega Donjuan','Ignacio Machain 983','013322565763',NULL,'dani.ortega9607@gmail.com',NULL,'2018-12-13 02:54:25','2018-12-13 02:54:25',NULL),(3,'Proveedor nuevo',NULL,NULL,NULL,NULL,NULL,'2018-12-15 01:13:40','2018-12-15 01:13:40',NULL);
/*!40000 ALTER TABLE `suppliers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_permissions`
--

DROP TABLE IF EXISTS `user_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_permissions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  `editor_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_permissions`
--

LOCK TABLES `user_permissions` WRITE;
/*!40000 ALTER TABLE `user_permissions` DISABLE KEYS */;
INSERT INTO `user_permissions` VALUES (1,1,1,1,NULL,NULL),(2,1,2,1,NULL,NULL),(3,1,3,1,NULL,NULL),(4,1,4,1,NULL,NULL),(10,2,1,1,'2018-12-15 11:15:55','2018-12-15 11:15:55');
/*!40000 ALTER TABLE `user_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_type` tinyint(4) NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `customer_id` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,1,'Daniel Ortega','dani.ortega9607@gmail.com',NULL,'$2y$10$6hnc.Mqq4KAjIPbz6SdoIepv3A26VyDr0mi.SoN1t/HwgQwLSymna',NULL,NULL,NULL,NULL,NULL),(2,2,'Admin','admin@admin.com',NULL,'$2y$10$evTU4REO5yQmMqxashtNeuzgjbHQ/ZV/0E/wTjEoEjx2rnAkFDEUG',NULL,'2018-12-11 23:50:07','2018-12-15 11:16:00',NULL,NULL),(3,3,'Luis nuevo','cliente@admin.com',NULL,'$2y$10$P2m3.9aiw2mp1OzoxwZ58.8RixFiYlcYizNPHg5hB1TavLx2amzbW',NULL,'2018-12-15 12:27:51','2018-12-15 12:33:22',NULL,16),(4,3,'Otro Cliente','otro@admin.com',NULL,'$2y$10$s8xK2IXUQaxB9p6wk.xsDOo/2d4KxithNUT5LFXYqfSmVQHPdlJba',NULL,'2018-12-15 12:33:51','2018-12-15 12:33:51',NULL,17),(5,3,'Nuevo','nuevo@admin.com',NULL,'$2y$10$sLzPHdYrwKEEjAaZWrAfDeCrQlqS3Q2S7dUBoXGDEJWoznyIRe9.u',NULL,'2018-12-15 12:34:38','2018-12-15 12:34:38',NULL,18),(6,3,'Jorge','jorge@admin.com',NULL,'$2y$10$o3u9StTRWDSNNj6.smvjiuf3B80HLTfI92JfRJdCID5aYEQdte12W',NULL,'2018-12-15 12:35:23','2018-12-15 12:35:23',NULL,19),(7,3,'Juan Oswaldo','oswa@admin.com',NULL,'$2y$10$M.UKmxM7F2WlprHmDLfkiOzNQt8yweDwDSvnLeFFhMCyZNBixeSHS',NULL,'2018-12-16 05:26:03','2018-12-16 05:26:03',NULL,20);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-12-15 17:43:14
